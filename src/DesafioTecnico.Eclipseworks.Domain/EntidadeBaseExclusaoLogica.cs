﻿namespace DesafioTecnico.Eclipseworks.Domain
{
    public class EntidadeBaseExclusaoLogica : EntidadeBase
    {
        public EntidadeBaseExclusaoLogica()
        {
            Excluido = false;
        }

        public bool Excluido { get; set; }
    }
}
