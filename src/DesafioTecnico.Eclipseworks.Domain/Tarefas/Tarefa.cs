﻿using DesafioTecnico.Eclipseworks.Domain.Projetos;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Commands;

namespace DesafioTecnico.Eclipseworks.Domain.Tarefas;

public sealed class Tarefa : EntidadeBaseExclusaoLogica
{
    public string Titulo { get; private set; }
    public string Descricao { get; private set; }

    public DateTime DataVencimento { get; private set; }
    public StatusTarefa Status { get; private set; } = StatusTarefa.Pendente;
    public PrioridadeTarefa Prioridade { get; private set; }

    public Guid IdProjeto { get; private set; }
    public Projeto Projeto { get; set; }

    public List<ComentarioTarefa> Comentarios { get; init; } = [];
    public List<HistoricoTarefa> HistoricoAtualizacoesTarefa { get; init; } = [];
    
    public void AdicionarInformacoes(AdicionarTarefaCommand command)
    {
        IdProjeto = command.IdProjeto;

        Titulo = command.Titulo;
        Status = command.Status;
        Prioridade = command.Prioridade;
        Descricao = command.Descricao;
        DataVencimento = command.DataVencimento;
    }

    public void AtualizarInformacoes(AtualizarTarefaCommand command)
    {
        Titulo = command.Titulo;
        Status = command.Status;
        Descricao = command.Descricao;
        DataVencimento = command.DataVencimento;
    }
}