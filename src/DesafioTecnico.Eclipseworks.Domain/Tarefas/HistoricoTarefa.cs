﻿using DesafioTecnico.Eclipseworks.Domain.Usuarios;

namespace DesafioTecnico.Eclipseworks.Domain.Tarefas;

public class HistoricoTarefa : EntidadeBase
{
    public Guid IdUsuario { get; init; }
    public Usuario Usuario { get; init; }

    public Guid IdTarefa { get; init; }
    public Tarefa Tarefa { get; init; }

    public string DadosAlterados { get; init; } 
    public string Comentario { get; init; }
}