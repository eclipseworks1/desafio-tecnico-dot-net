﻿namespace DesafioTecnico.Eclipseworks.Domain.Tarefas;

public enum PrioridadeTarefa
{
    Alta = 0,
    Media = 1,
    Baixa = 2
}