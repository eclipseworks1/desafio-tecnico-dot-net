﻿using System.ComponentModel;

namespace DesafioTecnico.Eclipseworks.Domain.Tarefas;

public enum StatusTarefa
{
    [Description("Pendente")]
    Pendente = 0,

    [Description("Em andamento")]
    EmAndamento = 1,

    [Description("Concluído")]
    Concluido = 2
}