﻿using DesafioTecnico.Eclipseworks.Domain.Usuarios;

namespace DesafioTecnico.Eclipseworks.Domain.Tarefas;

public class ComentarioTarefa : EntidadeBase
{
    public Guid IdTarefa { get; init; }
    public Tarefa Tarefa { get; set; }

    public Guid IdUsuario { get; init; }
    public Usuario Usuario { get; init; }

    public string Comentario { get; init; }
}