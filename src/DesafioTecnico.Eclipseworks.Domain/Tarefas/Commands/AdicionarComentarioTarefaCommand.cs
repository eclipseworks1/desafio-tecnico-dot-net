﻿using MediatR;

namespace DesafioTecnico.Eclipseworks.Domain.Tarefas.Commands;

public record AdicionarComentarioTarefaInputModel
{
    public string Comentario { get; init; }        
}

public sealed record AdicionarComentarioTarefaCommand(Guid IdProjeto, Guid IdTarefa) : AdicionarComentarioTarefaInputModel, IRequest { }