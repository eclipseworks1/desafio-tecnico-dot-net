﻿using MediatR;

namespace DesafioTecnico.Eclipseworks.Domain.Tarefas.Commands;

public record AtualizarTarefaInputModel
{
    public string Titulo { get; init; }
    public string Descricao { get; init; }
    public DateTime DataVencimento { get; init; }
    public StatusTarefa Status { get; init; }
}

public sealed record AtualizarTarefaCommand(Guid IdProjeto, Guid IdTarefa) : AtualizarTarefaInputModel, IRequest { }