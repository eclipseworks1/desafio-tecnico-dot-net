﻿using MediatR;

namespace DesafioTecnico.Eclipseworks.Domain.Tarefas.Commands;

public sealed record RemoverTarefaCommand(Guid IdProjeto, Guid IdTarefa) : IRequest { }