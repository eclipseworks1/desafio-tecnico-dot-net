﻿using MediatR;

namespace DesafioTecnico.Eclipseworks.Domain.Tarefas.Commands;

public record AdicionarTarefaInputModel
{
    public string Titulo { get; init; }
    public string Descricao { get; init; }
    public DateTime DataVencimento { get; init; }
    public StatusTarefa Status { get; init; }
    public PrioridadeTarefa Prioridade { get; init; }
}

public sealed record AdicionarTarefaCommand(Guid IdProjeto) : AdicionarTarefaInputModel, IRequest { }