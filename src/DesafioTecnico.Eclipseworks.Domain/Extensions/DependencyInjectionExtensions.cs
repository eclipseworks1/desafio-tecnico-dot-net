﻿using Microsoft.Extensions.DependencyInjection;

namespace DesafioTecnico.Eclipseworks.Domain.Extensions;

public static class DependencyInjectionExtensions
{
    public static IServiceCollection RegisterDomainDependencies(this IServiceCollection services)
    {
        services.AddScoped<IUser, AspNetUser>();
        return services;
    }
}
