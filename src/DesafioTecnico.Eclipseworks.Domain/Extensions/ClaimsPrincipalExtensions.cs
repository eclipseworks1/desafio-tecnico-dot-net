﻿using System.Security.Claims;

namespace DesafioTecnico.Eclipseworks.Domain.Extensions;

public static class ClaimsPrincipalExtensions
{
    public static Guid GetUserId(this ClaimsPrincipal principal)
    {
        if (principal == null)
        {
            throw new ArgumentException(null, nameof(principal));
        }

        return Guid.Parse(principal.FindFirst(ClaimTypes.NameIdentifier)?.Value);
    }
}
