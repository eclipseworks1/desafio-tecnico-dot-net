﻿namespace DesafioTecnico.Eclipseworks.Domain.Usuarios.Queries
{
    public record ResumoProjetosCriadosPorUsuarioPorPeriodo
    {
        public Guid IdUsuario { get; init; }
        public string Email { get; init; }

        public int TotalProjetosCriados { get; set; }
        public int TotalDiasPesquisados { get; set; }

        public DateTime DataInicioPeriodo { get; init; }
        public DateTime DataFimPeriodo = DateTime.Now.Date;

        public decimal MediaProjetosCriadosPorUsuarioPorPeriodo => TotalDiasPesquisados > 0
            ? Math.Round((decimal)TotalProjetosCriados / TotalDiasPesquisados,2)
            : 0;
    }
}