﻿namespace DesafioTecnico.Eclipseworks.Domain.Usuarios.Queries
{
    public class ResumoComentariosAdicionadosPorUsuarioPorPeriodo
    {
        public Guid IdUsuario { get; init; }
        public string Email { get; init; }

        public int TotalComentariosAdicionados { get; set; }
        public int TotalDiasPesquisados { get; set; }

        public DateTime DataInicioPeriodo { get; init; }
        public DateTime DataFimPeriodo = DateTime.Now.Date;

        public decimal MediaComentariosAdicionadosPorUsuarioPorPeriodo => TotalDiasPesquisados > 0
            ? Math.Round((decimal)TotalComentariosAdicionados / TotalDiasPesquisados, 2)
            : 0;
    }
}
