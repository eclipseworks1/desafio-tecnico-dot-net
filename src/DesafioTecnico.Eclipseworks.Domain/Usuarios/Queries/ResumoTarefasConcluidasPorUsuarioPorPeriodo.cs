﻿namespace DesafioTecnico.Eclipseworks.Domain.Usuarios.Queries
{
    public record ResumoTarefasConcluidasPorUsuarioPorPeriodo
    {
        public Guid IdUsuario { get; init; }
        public string Email { get; init; }

        public int TotalTarefasConcluidas { get; set; }
        public int TotalDiasPesquisados { get; set; }

        public DateTime DataInicioPeriodo { get; init; }
        public DateTime DataFimPeriodo = DateTime.Now.Date;

        public decimal MediaTarefasConcluidasPorUsuarioPorPeriodo => TotalDiasPesquisados > 0
            ? Math.Round((decimal)TotalTarefasConcluidas / TotalDiasPesquisados, 2)
            : 0;
    }
}