﻿using DesafioTecnico.Eclipseworks.Domain.Projetos;
using DesafioTecnico.Eclipseworks.Domain.Tarefas;

namespace DesafioTecnico.Eclipseworks.Domain.Usuarios;

public class Usuario(string nome, string email) : EntidadeBase
{
    public string Nome { get; init; } = nome;
    public string Email { get; init; } = email;

    public List<Projeto> Projetos { get; init; } = [];
    public List<ComentarioTarefa> Comentarios { get; init; } = [];
    public List<HistoricoTarefa> HistoricoAtualizacoesTarefa { get; set; } = [];
}