﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace DesafioTecnico.Eclipseworks.Domain.Constantes;

public static class ApplicationSecurity
{
    // Todo : alterar para arquivo de configuracao ou variavel de ambiente
    public const int TotalDiasExpiracaoToken = 30;
    public const string Issuer = "eclipseworks";
    public const string Audience = "eclipseworks";
    public const string SecretKey = "eclipseworks_tokenserver_eclipseworks_tokenserver_eclipseworks_tokenserver";

}

public class SigningCredentialsConfiguration
{
    public SigningCredentials SigningCredentials { get; }
    public SymmetricSecurityKey Key { get; private set; }

    public SigningCredentialsConfiguration(string secretKey)
    {
        Key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
        SigningCredentials = new SigningCredentials(Key, SecurityAlgorithms.HmacSha256);
    }
}
