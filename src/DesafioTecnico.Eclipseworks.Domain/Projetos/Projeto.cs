﻿using DesafioTecnico.Eclipseworks.Domain.Tarefas;
using DesafioTecnico.Eclipseworks.Domain.Usuarios;

namespace DesafioTecnico.Eclipseworks.Domain.Projetos;

public sealed class Projeto: EntidadeBaseExclusaoLogica
{
    public string Nome { get; init; } 

    public Usuario Usuario { get; init; }
    public Guid IdUsuario { get; init; } 

    public List<Tarefa> Tarefas { get; init; } = [];
}