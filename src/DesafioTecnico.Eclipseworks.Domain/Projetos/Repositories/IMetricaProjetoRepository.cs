﻿using DesafioTecnico.Eclipseworks.Domain.Usuarios.Queries;

namespace DesafioTecnico.Eclipseworks.Domain.Projetos.Repositories
{
    public interface IMetricaProjetoRepository
    {
        Task<IEnumerable<ResumoProjetosCriadosPorUsuarioPorPeriodo>> RecuperarTotalProjetosCriadosPorUsuarioPorPeriodo(int totalDias);
        Task<IEnumerable<ResumoTarefasConcluidasPorUsuarioPorPeriodo>> RecuperarTotalTarefasConcluidasPorUsuarioPorPeriodo(int totalDias);
        Task<IEnumerable<ResumoComentariosAdicionadosPorUsuarioPorPeriodo>> RecuperarTotalComentariosAdicionadosPorUsuarioPorPeriodo(int totalDias);        
    }
}
