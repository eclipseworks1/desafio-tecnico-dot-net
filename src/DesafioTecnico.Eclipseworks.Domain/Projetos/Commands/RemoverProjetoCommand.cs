﻿using MediatR;

namespace DesafioTecnico.Eclipseworks.Domain.Projetos.Commands;

public sealed record RemoverProjetoCommand(Guid IdProjeto) : IRequest
{
}
