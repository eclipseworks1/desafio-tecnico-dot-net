﻿using MediatR;

namespace DesafioTecnico.Eclipseworks.Domain.Projetos.Commands;

public sealed record AdicionarProjetoCommand(string Nome) : IRequest { }
