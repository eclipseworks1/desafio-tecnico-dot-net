﻿using DesafioTecnico.Eclipseworks.Business;
using DesafioTecnico.Eclipseworks.Domain;
using DesafioTecnico.Eclipseworks.Domain.Tarefas;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Commands;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Repositories;
using DesafioTecnico.Eclipseworks.Services.Validators.Tarefas;
using MediatR;

namespace DesafioTecnico.Eclipseworks.Services.Tarefas.Commands;

public class AdicionarComentarioTarefaCommandHandler(AdicionarComentarioTarefaValidator validator,
                                                     IDomainNotificationAppService domainNotificationAppService,
                                                     ITarefaRepository tarefaRepository,
                                                     IUser usuario) : IRequestHandler<AdicionarComentarioTarefaCommand>
{
    private readonly IUser _usuario = usuario;
    private readonly ITarefaRepository _tarefaRepository = tarefaRepository;
    private readonly AdicionarComentarioTarefaValidator _validator = validator;
    private readonly IDomainNotificationAppService _domainNotificationAppService = domainNotificationAppService;

    public async Task Handle(AdicionarComentarioTarefaCommand request, CancellationToken cancellationToken)
    {
        if (!await _validator.OperacaoValida(request))
        {
            _domainNotificationAppService.Add(_validator.CriticasNegocio);
            return;
        }

        var comentario = new ComentarioTarefa
        {
            IdUsuario = _usuario.Id,
            IdTarefa = request.IdTarefa,
            Comentario = request.Comentario
        };

        await _tarefaRepository.AdicionarComentario(comentario);
    }
}