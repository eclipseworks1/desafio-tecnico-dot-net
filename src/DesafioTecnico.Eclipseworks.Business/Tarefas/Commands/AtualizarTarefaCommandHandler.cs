﻿using DesafioTecnico.Eclipseworks.Domain.Tarefas.Commands;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Repositories;
using DesafioTecnico.Eclipseworks.Services.Validators.Tarefas;
using MediatR;

namespace DesafioTecnico.Eclipseworks.Business.Tarefas.Commands;

public class AtualizarTarefaCommandHandler(IDomainNotificationAppService domainNotificationAppService,
                                           AtualizarTarefaValidator validator,
                                           ITarefaRepository tarefaRepository) : IRequestHandler<AtualizarTarefaCommand>
{
    private readonly AtualizarTarefaValidator _validator = validator;
    private readonly ITarefaRepository _tarefaRepository = tarefaRepository;
    private readonly IDomainNotificationAppService _domainNotificationAppService = domainNotificationAppService;

    public async Task Handle(AtualizarTarefaCommand request, CancellationToken cancellationToken)
    {
        if (!await _validator.OperacaoValida(request))
        {
            _domainNotificationAppService.Add(_validator.CriticasNegocio);
            return;
        }

        var tarefa = await _tarefaRepository.RecuperarPorIdAsync(request.IdProjeto, request.IdTarefa);
        tarefa.AtualizarInformacoes(request);

        await _tarefaRepository.Atualizar(tarefa);
    }
}