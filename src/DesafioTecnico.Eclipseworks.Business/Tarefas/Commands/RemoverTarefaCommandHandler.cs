﻿using DesafioTecnico.Eclipseworks.Business;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Commands;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Repositories;
using DesafioTecnico.Eclipseworks.Services.Validators.Tarefas;
using MediatR;

namespace DesafioTecnico.Eclipseworks.Domain.Tarefas.AdicionarTarefa.Commands;

public class RemoverTarefaCommandHandler(RemoverTarefaValidator validator,
                                         IDomainNotificationAppService domainNotificationAppService,
                                         ITarefaRepository tarefaRepository) : IRequestHandler<RemoverTarefaCommand>
{
    private readonly RemoverTarefaValidator _validator = validator;
    private readonly ITarefaRepository _tarefaRepository = tarefaRepository;
    private readonly IDomainNotificationAppService _domainNotificationAppService = domainNotificationAppService;

    public async Task Handle(RemoverTarefaCommand request, CancellationToken cancellationToken)
    {
        if (!await _validator.OperacaoValida(request))
        {
            _domainNotificationAppService.Add(_validator.CriticasNegocio);
            return;
        }

        await _tarefaRepository.Excluir(request.IdProjeto, request.IdTarefa);
    }
}