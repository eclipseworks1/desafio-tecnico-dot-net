﻿using DesafioTecnico.Eclipseworks.Business;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Commands;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Repositories;
using DesafioTecnico.Eclipseworks.Services.Validators.Tarefas;
using MediatR;

namespace DesafioTecnico.Eclipseworks.Domain.Tarefas.AdicionarTarefa.Commands;

public class AdicionarTarefaCommandHandler(IDomainNotificationAppService domainNotificationAppService,
    ITarefaRepository tarefaRepository,
    AdicionarTarefaValidator validator) : IRequestHandler<AdicionarTarefaCommand>
{
    private readonly ITarefaRepository _tarefaRepository = tarefaRepository;
    private readonly IDomainNotificationAppService _domainNotificationAppService = domainNotificationAppService;
    private readonly AdicionarTarefaValidator _validator = validator;

    public async Task Handle(AdicionarTarefaCommand request, CancellationToken cancellationToken)
    {
        if (!await _validator.OperacaoValida(request))
        {
            _domainNotificationAppService.Add(_validator.CriticasNegocio);
            return;
        }

        var tarefa = new Tarefa();
        tarefa.AdicionarInformacoes(request);

        await _tarefaRepository.Adicionar(tarefa);
    }
}