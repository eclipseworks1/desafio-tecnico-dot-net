﻿using DesafioTecnico.Eclipseworks.Business;
using DesafioTecnico.Eclipseworks.Domain.Projetos;
using DesafioTecnico.Eclipseworks.Domain.Projetos.Commands;
using DesafioTecnico.Eclipseworks.Domain.Projetos.Repositories;
using DesafioTecnico.Eclipseworks.Services.Validators.Projetos;
using MediatR;

namespace DesafioTecnico.Eclipseworks.Domain.Tarefas.AdicionarTarefa.Commands;

public class AdicionarProjetoCommandHandler : IRequestHandler<AdicionarProjetoCommand>
{
    private readonly IUser _usuarioLogado;
    private readonly IProjetoRepository _repository;
    private readonly IDomainNotificationAppService _domainNotificationAppService;
    private readonly AdicionarProjetoValidator _validator;

    public AdicionarProjetoCommandHandler(IDomainNotificationAppService domainNotificationAppService, IProjetoRepository repository, IUser usuarioLogado, AdicionarProjetoValidator validator)
    {
        _repository = repository;
        _domainNotificationAppService = domainNotificationAppService;
        _usuarioLogado = usuarioLogado;
        _validator = validator;
    }

    public async Task Handle(AdicionarProjetoCommand request, CancellationToken cancellationToken)
    {
        if (!_validator.OperacaoValida(request))
        {
            _domainNotificationAppService.Add(_validator.CriticasNegocio);
            return;
        }

        var projeto = new Projeto
        {
            Nome = request.Nome,
            IdUsuario = _usuarioLogado.Id
        };

        await _repository.AdicionarAsync(projeto);
    }
}