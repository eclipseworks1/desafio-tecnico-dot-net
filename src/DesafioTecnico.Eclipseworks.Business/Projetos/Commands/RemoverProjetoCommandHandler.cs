﻿using DesafioTecnico.Eclipseworks.Business;
using DesafioTecnico.Eclipseworks.Domain.Projetos.Commands;
using DesafioTecnico.Eclipseworks.Domain.Projetos.Repositories;
using DesafioTecnico.Eclipseworks.Services.Validators.Projetos;
using MediatR;

namespace DesafioTecnico.Eclipseworks.Domain.Tarefas.AdicionarTarefa.Commands;

public class RemoverProjetoCommandHandler(IDomainNotificationAppService domainNotificationAppService,
                                          IProjetoRepository repository,
                                          RemoverProjetoValidator validator) : IRequestHandler<RemoverProjetoCommand>
{
    private readonly IProjetoRepository _repository = repository;
    private readonly RemoverProjetoValidator _validator = validator;
    private readonly IDomainNotificationAppService _domainNotificationAppService = domainNotificationAppService;

    public async Task Handle(RemoverProjetoCommand request, CancellationToken cancellationToken)
    {
        if (!await _validator.OperacaoValida(request))
        {
            _domainNotificationAppService.Add(_validator.CriticasNegocio);
            return;
        }

        await _repository.ExcluirAsync(request.IdProjeto);
    }
}