﻿using DesafioTecnico.Eclipseworks.Domain.Projetos;
using DesafioTecnico.Eclipseworks.Domain.Projetos.Commands;
using DesafioTecnico.Eclipseworks.Services.Extensions;
using FluentValidation;
using FluentValidation.Results;

namespace DesafioTecnico.Eclipseworks.Services.Validators.Projetos;

public class AdicionarProjetoValidator : AbstractValidator<AdicionarProjetoCommand>
{
    private ValidationResult ValidationResult { get; set; }
    public string[] CriticasNegocio => ValidationResult.Criticas();

    public AdicionarProjetoValidator()
    {
        RuleFor(a => a.Nome)
          .NotEmpty()
          .WithMessage("O campo Nome é obrigatório.");

        RuleFor(a => a.Nome)
          .MaximumLength(255)
          .WithMessage("O campo Nome deve possuir no máximo 255 caracteres.")
          .When(a => !string.IsNullOrEmpty(a.Nome));
    }

    public bool OperacaoValida(AdicionarProjetoCommand inputModel)
    {
        ValidationResult = Validate(inputModel);
        return ValidationResult.IsValid;
    }
}


