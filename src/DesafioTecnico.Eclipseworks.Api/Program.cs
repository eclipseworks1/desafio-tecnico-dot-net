using DesafioTecnico.Eclipseworks.Api.Extensions;
using DesafioTecnico.Eclipseworks.Business.Extensions;
using DesafioTecnico.Eclipseworks.Domain.Extensions;
using DesafioTecnico.Eclipseworks.Repository.Extensions;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services
    .RegisterDomainDependencies()
    .RegisterApiDependencies(builder.Configuration)
    .RegisterBusinessDependencies()
    .RegisterRepositoryDependencies();

var app = builder.Build();

app.RegisterMiddlewares()
   .RegisterSwaggerRoute();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseRouting();
app.UseAuthorization();
app.UseCors("*");

app.MapControllers();
app.MigrateDatabase();

app.Run();