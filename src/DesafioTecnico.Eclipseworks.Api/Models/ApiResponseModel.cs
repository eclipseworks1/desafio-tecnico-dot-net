﻿namespace DesafioTecnico.Eclipseworks.Api.Models;

internal sealed record ApiResponseModel<T> where T : class
{
    public T Data { get; }
    public IReadOnlyCollection<string> Errors { get; }

    private ApiResponseModel(T data, IReadOnlyCollection<string> errors)
    {
        Data = data;
        Errors = errors;
    }

    public static ApiResponseModel<T> CreateSuccessResponse(T data = null)
    {
        return new ApiResponseModel<T>(data: data, errors: null);
    }

    public static ApiResponseModel<T> CreateNotFoundResponse(string message = "Recurso não encontrado")
    {
        return new ApiResponseModel<T>(data: null, new[] { message });
    }

    public static ApiResponseModel<T> CreateBadRequestResponse(IReadOnlyCollection<string> errors)
    {
        return new ApiResponseModel<T>(data: null, errors: errors);
    }

    public static ApiResponseModel<T> CreateInternalServerErrorResponse(string message = "Atenção, erro inesperado ao realizar a operação!")
    {
        return new ApiResponseModel<T>(data: null, errors: new[] { message });
    }
}
