﻿using DesafioTecnico.Eclipseworks.Domain.Tarefas.Commands;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Text.Json;

namespace DesafioTecnico.Eclipseworks.Api.Filters;

public class HistoricoAtualizacaoTarefaFilter() : IAsyncActionFilter
{
    public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
    {
        var resultContext = await next();

        if (MetodoExecutadoComSucesso(resultContext))
        {
            var mediator = context.HttpContext.RequestServices.GetRequiredService<IMediator>();

            var command = RecuperarParametrosEntrada(context);
            await mediator.Send(command);
        }
    }

    private static bool MetodoExecutadoComSucesso(ActionExecutedContext resultContext)
    {
        return resultContext.Exception == null
            && resultContext.Result is NoContentResult objectResult
            && objectResult.StatusCode >= 200
            && objectResult.StatusCode < 300;
    }

    private static AdicionarHistoricoTarefaCommand RecuperarParametrosEntrada(ActionExecutingContext context)
    {
        Guid idTarefa = Guid.Empty;

        string dadosAlterados = string.Empty;
        string comentarioTarefa = string.Empty;

        if (context.HttpContext.Request.RouteValues.TryGetValue("idTarefa", out var idTarefaValue))
            _ = Guid.TryParse(idTarefaValue?.ToString(), out idTarefa);

        if (context.ActionArguments.TryGetValue("tarefaInputModel" , out var tarefaInputModel))
        {
            dadosAlterados = JsonSerializer.Serialize(tarefaInputModel);
        }

        if (context.ActionArguments.TryGetValue("comentarioInputModel", out var comentarioInputModel))
        {
            comentarioTarefa = ((AdicionarComentarioTarefaInputModel)comentarioInputModel).Comentario;
        }

        return new AdicionarHistoricoTarefaCommand
        {
            IdTarefa = idTarefa,
            DadosAlterados = dadosAlterados,                
            Comentario = comentarioTarefa
        };
    }
}