using DesafioTecnico.Eclipseworks.Api.Controllers.Base;
using DesafioTecnico.Eclipseworks.Api.Models;
using DesafioTecnico.Eclipseworks.Business;
using DesafioTecnico.Eclipseworks.Domain;
using DesafioTecnico.Eclipseworks.Domain.Exceptions;
using DesafioTecnico.Eclipseworks.Domain.Projetos;
using DesafioTecnico.Eclipseworks.Domain.Projetos.Commands;
using DesafioTecnico.Eclipseworks.Domain.Projetos.Queries;
using DesafioTecnico.Eclipseworks.Domain.Projetos.Repositories;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace DesafioTecnico.Eclipseworks.Api.Controllers;

[Route("api/v1/projetos")]
public class ProjectsController(IDomainNotificationAppService domainNotificationService, 
                                IMediator mediator, 
                                IProjetoRepository repository,
                                IUser usuarioLogado) : BaseAuthorizedApiController(domainNotificationService)
{
    private readonly IMediator _mediator = mediator;
    private readonly IUser _usuarioLogado = usuarioLogado;        
    private readonly IProjetoRepository _projetoRepository = repository;        

    [HttpGet]
    [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(ApiResponseModel<IEnumerable<Projeto>>))]
    [ProducesResponseType((int)HttpStatusCode.BadRequest, Type = typeof(ApiResponseModel<string>))]
    [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ApiResponseModel<string>))]
    public async Task<IActionResult> GetAll()
    {
        var projetos = await _projetoRepository.RecuperarTodosPorUsuario(_usuarioLogado.Id);

        ObjectNotFoundException.LancarExcecaoSeNullOuVazio(projetos);

        var response = projetos?.Select(projeto => new ProjetoResponseModel(projeto));
        return Ok(response);
    }

    [HttpPost]
    [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(ApiResponseModel<Projeto>))]
    [ProducesResponseType((int)HttpStatusCode.BadRequest, Type = typeof(ApiResponseModel<string>))]
    [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ApiResponseModel<string>))]
    public async Task<IActionResult> Post([FromBody] AdicionarProjetoCommand command)
    {
        await _mediator.Send(command);
        return NoContent();
    }

    [HttpDelete]
    [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(ApiResponseModel<Projeto>))]
    [ProducesResponseType((int)HttpStatusCode.BadRequest, Type = typeof(ApiResponseModel<string>))]
    [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ApiResponseModel<string>))]
    public async Task<IActionResult> Delete([FromBody] RemoverProjetoCommand command)
    {
        await _mediator.Send(command);
        return NoContent();
    }
}