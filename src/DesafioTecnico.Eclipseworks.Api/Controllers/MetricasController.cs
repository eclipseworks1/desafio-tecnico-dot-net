using DesafioTecnico.Eclipseworks.Api.Controllers.Base;
using DesafioTecnico.Eclipseworks.Business;
using DesafioTecnico.Eclipseworks.Domain.Projetos.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DesafioTecnico.Eclipseworks.Api.Controllers;

[Authorize(Policy = "PerfilAcessoPolicy")]
[Route("api/v1/usuarios/metricas/medias")]
public class MetricasController(IDomainNotificationAppService domainNotificationService,
                                IMetricaProjetoRepository repository) : BaseAuthorizedApiController(domainNotificationService)
{
    private readonly IMetricaProjetoRepository _repository = repository;

    [HttpGet]
    [Route("tarefas-concluidas/{totalDiasRelatorio:int}")]
    public async Task<IActionResult> TarefasConcluidasPorUsuario([FromRoute] int totalDiasRelatorio = 30)
    {
        var response = await _repository.RecuperarTotalTarefasConcluidasPorUsuarioPorPeriodo(totalDiasRelatorio);
        return Ok(response);
    }

    [HttpGet]
    [Route("projetos-criados/{totalDiasRelatorio:int}")]
    public async Task<IActionResult> ProjetosCriadosPorUsuario([FromRoute] int totalDiasRelatorio)
    {
        var response = await _repository.RecuperarTotalProjetosCriadosPorUsuarioPorPeriodo(totalDiasRelatorio);
        return Ok(new { totalProjetosCriados = response });
    }

    [HttpGet]
    [Route("comentarios-adicionados/{totalDiasRelatorio:int}")]
    public async Task<IActionResult> ComentariosAdicionadosPorUsuario([FromRoute] int totalDiasRelatorio)
    {
        var response = await _repository.RecuperarTotalComentariosAdicionadosPorUsuarioPorPeriodo(totalDiasRelatorio);
        return Ok(new { totalComenttariosAdicionados = response });
    }
}