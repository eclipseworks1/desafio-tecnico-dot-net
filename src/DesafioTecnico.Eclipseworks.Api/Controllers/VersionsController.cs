using Microsoft.AspNetCore.Mvc;

namespace DesafioTecnico.Eclipseworks.Api.Controllers;

[ApiController]
[Route("api/versions")]
public class VersionsController : ControllerBase
{
    [HttpGet]
    public IActionResult Get() => Ok("1.1");
}