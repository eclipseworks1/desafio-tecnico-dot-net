using DesafioTecnico.Eclipseworks.Api.Controllers.Base;
using DesafioTecnico.Eclipseworks.Api.Filters;
using DesafioTecnico.Eclipseworks.Api.Models;
using DesafioTecnico.Eclipseworks.Business;
using DesafioTecnico.Eclipseworks.Domain.Exceptions;
using DesafioTecnico.Eclipseworks.Domain.Projetos;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Commands;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Queries;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Repositories;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DesafioTecnico.Eclipseworks.Api.Controllers;

[Route("api/v1/projetos/{idProjeto:guid}/tarefas")]
public class TarefasController(ITarefaRepository tarefaRepository,
                               IMediator mediator, 
                               IDomainNotificationAppService domainNotificationService) : BaseAuthorizedApiController(domainNotificationService)
{   
    private readonly IMediator _mediator = mediator;
    private readonly ITarefaRepository _tarefaRepository = tarefaRepository;

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ApiResponseModel<IEnumerable<Projeto>>))]
    [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiResponseModel<string>))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponseModel<string>))]
    public async Task<IActionResult> GetAll([FromRoute] Guid idProjeto)
    {
        var tarefas = await _tarefaRepository.RecuperarTodas(idProjeto);

        ObjectNotFoundException.LancarExcecaoSeNullOuVazio(tarefas);

        var response = tarefas?.Select(tarefa => new TarefaResponseModel(tarefa));
        return Ok(response);
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ApiResponseModel<Projeto>))]
    [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiResponseModel<string>))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponseModel<string>))]
    public async Task<IActionResult> Post([FromBody] AdicionarTarefaInputModel inputModel, [FromRoute] Guid idProjeto)
    {
        var command = new AdicionarTarefaCommand(idProjeto)
        {
            DataVencimento = inputModel.DataVencimento,
            Descricao = inputModel.Descricao,
            Status = inputModel.Status,
            Titulo = inputModel.Titulo,
            Prioridade = inputModel.Prioridade
        };

        await _mediator.Send(command);
        return NoContent();
    }

    [ServiceFilter(typeof(HistoricoAtualizacaoTarefaFilter))]
    [HttpPut("{idTarefa:guid}")]        
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ApiResponseModel<Projeto>))]
    [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiResponseModel<string>))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponseModel<string>))]
    public async Task<IActionResult> Put([FromBody] AtualizarTarefaInputModel tarefaInputModel, [FromRoute] Guid idProjeto, [FromRoute] Guid idTarefa)
    {
        var command = new AtualizarTarefaCommand(idProjeto, idTarefa)
        {
            DataVencimento = tarefaInputModel.DataVencimento,
            Descricao = tarefaInputModel.Descricao,
            Status = tarefaInputModel.Status,
            Titulo = tarefaInputModel.Titulo,
        };

        await _mediator.Send(command);
        return NoContent();
    }

    [HttpDelete("{idTarefa:guid}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ApiResponseModel<Projeto>))]
    [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiResponseModel<string>))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponseModel<string>))]
    public async Task<IActionResult> Delete([FromRoute] Guid idProjeto, [FromRoute] Guid idTarefa)
    {
        var command = new RemoverTarefaCommand(idProjeto, idTarefa);
        await _mediator.Send(command);

        return NoContent();
    }

    [ServiceFilter(typeof(HistoricoAtualizacaoTarefaFilter))]
    [HttpPost("{idTarefa:guid}/comentarios")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ApiResponseModel<Projeto>))]
    [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiResponseModel<string>))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ApiResponseModel<string>))]
    public async Task<IActionResult> PostComentarios([FromBody] AdicionarComentarioTarefaInputModel comentarioInputModel, [FromRoute] Guid idProjeto, [FromRoute] Guid idTarefa)
    {
        var command = new AdicionarComentarioTarefaCommand(idProjeto,idTarefa)
        {
           Comentario = comentarioInputModel.Comentario,
        };

        await _mediator.Send(command);
        return NoContent();
    }
}