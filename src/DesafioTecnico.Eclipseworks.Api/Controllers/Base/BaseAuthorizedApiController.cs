﻿using DesafioTecnico.Eclipseworks.Api.Models;
using DesafioTecnico.Eclipseworks.Business;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DesafioTecnico.Eclipseworks.Api.Controllers.Base;

[Authorize]
[ApiController]    
public class BaseAuthorizedApiController(IDomainNotificationAppService domainNotificationService) : ControllerBase
{
    private readonly IDomainNotificationAppService _domainNotificationService = domainNotificationService;

    protected IActionResult Ok<T>(T data = null) where T : class
    {
        if (!_domainNotificationService.HasNotifications)
        {
            var response = ApiResponseModel<T>.CreateSuccessResponse(data);
            return new OkObjectResult(response);
        }
        else
        {
            var response = ApiResponseModel<string>.CreateBadRequestResponse(_domainNotificationService.GetNotifications());
            return new BadRequestObjectResult(response);
        }
    }

    protected new IActionResult NoContent()
    {
        if (!_domainNotificationService.HasNotifications)
        {
            return new NoContentResult();
        }
        else
        {
            var response = ApiResponseModel<string>.CreateBadRequestResponse(_domainNotificationService.GetNotifications());
            return new BadRequestObjectResult(response);
        }
    }
}