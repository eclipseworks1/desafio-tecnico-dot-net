﻿using DesafioTecnico.Eclipseworks.Api.Extensions;
using DesafioTecnico.Eclipseworks.Api.Models;
using DesafioTecnico.Eclipseworks.Domain.Exceptions;
using System.Net;

namespace DesafioTecnico.Eclipseworks.Api.Middlewares;

internal sealed class ExceptionHandlingMiddleware(RequestDelegate next)
{
    private readonly RequestDelegate _next = next;

    public async Task InvokeAsync(HttpContext context, ILogger<ExceptionHandlingMiddleware> logger)
    {
        context.Response.ContentType = "application/json";

        try
        {
            await _next(context);
        }
        catch (ObjectNotFoundException ex)
        {
            var response = ApiResponseModel<string>.CreateNotFoundResponse(ex.Message);
            await HandleErrorAsync(context, response, HttpStatusCode.NotFound);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Erro inesperado ao realizar a operação.");
            var response = ApiResponseModel<string>.CreateInternalServerErrorResponse();

            await HandleErrorAsync(context, response, HttpStatusCode.InternalServerError);
        }
    }

    private static async Task HandleErrorAsync(HttpContext httpcontext, ApiResponseModel<string> response, HttpStatusCode statusCode)
    {
        httpcontext.Response.StatusCode = (int)statusCode;
        await httpcontext.Response.WriteAsync(response.Serialize());
    }
}