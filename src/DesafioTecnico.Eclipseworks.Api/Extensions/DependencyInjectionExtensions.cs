﻿using DesafioTecnico.Eclipseworks.Api.Filters;
using DesafioTecnico.Eclipseworks.Api.Middlewares;
using DesafioTecnico.Eclipseworks.Domain.Constantes;
using DesafioTecnico.Eclipseworks.Repository.Context;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using System.Net;

namespace DesafioTecnico.Eclipseworks.Api.Extensions;

internal static class DependencyInjectionExtensions
{
    public static IServiceCollection RegisterApiDependencies(this IServiceCollection services, IConfiguration config)
    {
        services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

        services.AddLogging();

        services.AddMvc()
            .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

        services.AddScoped<HistoricoAtualizacaoTarefaFilter>();

        services.
            ConfigureSwaggerRoute()
            .ConfigureCors()
            .RegisterEntityFrameworkContext(config)
            .AddMvcSecurity();

        return services;
    }

    public static WebApplication RegisterMiddlewares(this WebApplication app)
    {
        app.UseMiddleware<ExceptionHandlingMiddleware>();
        return app;
    }

    public static WebApplication RegisterSwaggerRoute(this WebApplication app)
    {
        app.UseSwagger()
           .UseSwaggerUI();

        return app;
    }

    private static IServiceCollection ConfigureSwaggerRoute(this IServiceCollection services)
    {
        services.AddSwaggerGen(options =>
        {
            options.SwaggerDoc("v1", new OpenApiInfo { Title = "DesafioTecnico.Eclipseworks", Version = "v1" });

            options.IgnoreObsoleteActions();
            options.IgnoreObsoleteProperties();

            options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                Description = "",
                Name = "Authorization",
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.ApiKey
            });

            options.AddSecurityRequirement(new OpenApiSecurityRequirement()
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference{Type = ReferenceType.SecurityScheme,Id = "Bearer"}
                    },
                    new List<string>()
                }
            });
        });

        return services;
    }

    private static IServiceCollection ConfigureCors(this IServiceCollection services)
    {
        services.AddCors(options =>
        {
            options.AddPolicy("*",
                builder => builder
                .SetIsOriginAllowed((host) => true)
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());
        });

        return services;
    }

    private static IServiceCollection RegisterEntityFrameworkContext(this IServiceCollection services, IConfiguration config)
    {
        services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(config.GetConnectionString("DefaultConnection")));
        return services;
    }

    public static WebApplication MigrateDatabase(this WebApplication app)
    {
        using (var scope = app.Services.GetRequiredService<IServiceScopeFactory>().CreateScope())
        {
            var dbContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
            dbContext.Database.Migrate();
        }

        return app;
    }

    private static IServiceCollection AddMvcSecurity(this IServiceCollection services)
    {
        services.AddAuthentication(x =>
        {
            x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;

                x.Audience = ApplicationSecurity.Audience;
                x.ClaimsIssuer = ApplicationSecurity.Issuer;

                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ClockSkew = TimeSpan.Zero,

                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    RequireExpirationTime = true,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SigningCredentialsConfiguration(ApplicationSecurity.SecretKey).Key
                };

                x.TokenValidationParameters.ValidIssuers = new[]
                {
                    ApplicationSecurity.Issuer
                };
            });

        services.AddAuthorizationBuilder()
            .AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                .RequireAuthenticatedUser().Build())
            .AddPolicy("PerfilAcessoPolicy", policy =>
            {
                policy.RequireClaim("PerfilAcesso", "Gerente");
            });

        return services;
    }
}
