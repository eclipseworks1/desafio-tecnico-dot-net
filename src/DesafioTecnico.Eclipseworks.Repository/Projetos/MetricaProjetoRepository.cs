﻿using DesafioTecnico.Eclipseworks.Domain.Projetos.Repositories;
using DesafioTecnico.Eclipseworks.Domain.Tarefas;
using DesafioTecnico.Eclipseworks.Domain.Usuarios.Queries;
using DesafioTecnico.Eclipseworks.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace DesafioTecnico.Eclipseworks.Repository.Projetos
{
    public class MetricaProjetoRepository(ApplicationDbContext applicationDbContext) : IMetricaProjetoRepository
    {
        private readonly ApplicationDbContext _applicationDbContext = applicationDbContext;

        public async Task<IEnumerable<ResumoProjetosCriadosPorUsuarioPorPeriodo>> RecuperarTotalProjetosCriadosPorUsuarioPorPeriodo(int totalDias)
        {
            var dataInicio = RecuperarDataInicioIntervaloConsulta(totalDias);

            return await _applicationDbContext
                .Projetos.Include(a => a.Usuario)
                .Where(projeto => !projeto.Excluido
                            && projeto.DataCriacao.Date >= dataInicio.Date)
                .GroupBy(projeto => projeto.IdUsuario)
                .Select(group => new ResumoProjetosCriadosPorUsuarioPorPeriodo
                {
                    IdUsuario = group.Key,
                    Email = group.First().Usuario.Email,
                    DataInicioPeriodo = dataInicio.Date,
                    TotalDiasPesquisados = totalDias,
                    TotalProjetosCriados = group.Count()
                })
                .ToListAsync();
        }

        public async Task<IEnumerable<ResumoComentariosAdicionadosPorUsuarioPorPeriodo>> RecuperarTotalComentariosAdicionadosPorUsuarioPorPeriodo(int totalDias)
        {
            var dataInicio = RecuperarDataInicioIntervaloConsulta(totalDias);

            return await _applicationDbContext
                .Comentarios.Include(a => a.Tarefa.Projeto.Usuario)
                .Where(comentario => !comentario.Tarefa.Projeto.Excluido
                            && !comentario.Tarefa.Excluido                           
                            && comentario.DataCriacao.Date >= dataInicio.Date)
                .GroupBy(projeto => projeto.Tarefa.Projeto.IdUsuario)
                .Select(group => new ResumoComentariosAdicionadosPorUsuarioPorPeriodo
                {
                    IdUsuario = group.Key,
                    Email = group.First().Tarefa.Projeto.Usuario.Email,
                    DataInicioPeriodo = dataInicio.Date,
                    TotalDiasPesquisados = totalDias,
                    TotalComentariosAdicionados = group.Count()
                })
                .ToListAsync();
        }

        public async Task<IEnumerable<ResumoTarefasConcluidasPorUsuarioPorPeriodo>> RecuperarTotalTarefasConcluidasPorUsuarioPorPeriodo(int totalDias)
        {
            var dataInicio = RecuperarDataInicioIntervaloConsulta(totalDias);

            return await _applicationDbContext
                .Tarefas.Include(a => a.Projeto.Usuario)
                .Where(tarefa => !tarefa.Projeto.Excluido
                            && !tarefa.Excluido
                            && tarefa.Status == StatusTarefa.Concluido
                            && tarefa.DataCriacao.Date >= dataInicio.Date)
                .GroupBy(projeto => projeto.Projeto.IdUsuario)
                .Select(group => new ResumoTarefasConcluidasPorUsuarioPorPeriodo
                {
                    IdUsuario = group.Key,
                    Email = group.First().Projeto.Usuario.Email,
                    DataInicioPeriodo = dataInicio.Date,
                    TotalDiasPesquisados = totalDias,
                    TotalTarefasConcluidas = group.Count()
                })
                .ToListAsync();
        }

        private static DateTime RecuperarDataInicioIntervaloConsulta(int totalDias) => DateTime.Now.AddDays(totalDias * -1);
    }
}