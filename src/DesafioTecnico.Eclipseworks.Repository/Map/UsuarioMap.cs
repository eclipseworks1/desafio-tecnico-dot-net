﻿using DesafioTecnico.Eclipseworks.Domain.Usuarios;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DesafioTecnico.Eclipseworks.Repository.Map;

public class UsuarioMap : IEntityTypeConfiguration<Usuario>
{
    public void Configure(EntityTypeBuilder<Usuario> builder)
    {
        builder.ToTable("Usuarios");

        builder.HasKey(e => e.Id);
        builder.Property(p => p.Nome).IsRequired().HasMaxLength(255);
        builder.Property(p => p.Email).IsRequired().HasMaxLength(255);
        builder.Property(p => p.DataCriacao).IsRequired();

        builder.HasMany(t => t.Comentarios)
            .WithOne(p => p.Usuario)
            .HasForeignKey(t => t.IdUsuario)
            .OnDelete(DeleteBehavior.Cascade);

        builder.HasMany(t => t.HistoricoAtualizacoesTarefa)
            .WithOne(p => p.Usuario)
            .HasForeignKey(t => t.IdUsuario)
            .OnDelete(DeleteBehavior.Cascade);

        builder.HasMany(t => t.Projetos)
            .WithOne(p => p.Usuario)
            .HasForeignKey(t => t.IdUsuario)
            .OnDelete(DeleteBehavior.Cascade);

        builder.HasData
        (
            new Usuario("Usuario 01","user01@teste.com.br"),
            new Usuario("Usuario 02", "user02@teste.com.br"),
            new Usuario("Usuario 03", "user03@teste.com.br"),
            new Usuario("Usuario 04", "user04@teste.com.br"),
            new Usuario("Usuario 05", "user05@teste.com.br"),
            new Usuario("Gestor", "gestor@teste.com.br")
        );
    }
}