﻿using DesafioTecnico.Eclipseworks.Domain.Projetos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DesafioTecnico.Eclipseworks.Repository.Map;

public class ProjetoMap : IEntityTypeConfiguration<Projeto>
{
    public void Configure(EntityTypeBuilder<Projeto> builder)
    {
        builder.ToTable("Projetos");

        builder.HasKey(e => e.Id);
        builder.Property(p => p.Nome).IsRequired().HasMaxLength(255);
        builder.Property(p => p.DataCriacao).IsRequired();

        builder.Property(a => a.Excluido).IsRequired();

        builder.HasOne(a => a.Usuario)
            .WithMany(usuario => usuario.Projetos)
            .HasForeignKey(a => a.IdUsuario)
            .OnDelete(DeleteBehavior.NoAction);

        builder.HasMany(p => p.Tarefas)
            .WithOne(t => t.Projeto)
            .HasForeignKey(t => t.IdProjeto)
            .OnDelete(DeleteBehavior.Cascade);
    }
}
