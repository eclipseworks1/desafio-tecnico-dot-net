﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace DesafioTecnico.Eclipseworks.Repository.Migrations
{
    /// <inheritdoc />
    public partial class Initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Usuarios",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nome = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    DataCriacao = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuarios", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Projetos",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nome = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    IdUsuario = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DataCriacao = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Excluido = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projetos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projetos_Usuarios_IdUsuario",
                        column: x => x.IdUsuario,
                        principalTable: "Usuarios",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tarefas",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Descricao = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    DataVencimento = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Prioridade = table.Column<int>(type: "int", nullable: false),
                    IdProjeto = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DataCriacao = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Excluido = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tarefas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tarefas_Projetos_IdProjeto",
                        column: x => x.IdProjeto,
                        principalTable: "Projetos",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ComentarioTarefa",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IdTarefa = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IdUsuario = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Comentario = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    DataCriacao = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComentarioTarefa", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ComentarioTarefa_Tarefas_IdTarefa",
                        column: x => x.IdTarefa,
                        principalTable: "Tarefas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ComentarioTarefa_Usuarios_IdUsuario",
                        column: x => x.IdUsuario,
                        principalTable: "Usuarios",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HistoricoAlteracaoTarefa",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IdUsuario = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IdTarefa = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DadosAlterados = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Comentario = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    DataCriacao = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistoricoAlteracaoTarefa", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HistoricoAlteracaoTarefa_Tarefas_IdTarefa",
                        column: x => x.IdTarefa,
                        principalTable: "Tarefas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HistoricoAlteracaoTarefa_Usuarios_IdUsuario",
                        column: x => x.IdUsuario,
                        principalTable: "Usuarios",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Usuarios",
                columns: new[] { "Id", "DataCriacao", "Email", "Nome" },
                values: new object[,]
                {
                    { new Guid("2b403426-6eb6-4485-89bd-36e67ea0e5f3"), new DateTime(2023, 12, 4, 17, 53, 15, 369, DateTimeKind.Local).AddTicks(4809), "user05@teste.com.br", "Usuario 05" },
                    { new Guid("4c3017dc-ee78-4fb3-802f-20cb43323a51"), new DateTime(2023, 12, 4, 17, 53, 15, 369, DateTimeKind.Local).AddTicks(4807), "user04@teste.com.br", "Usuario 04" },
                    { new Guid("55b92a2e-def1-43db-a50f-102e2287d388"), new DateTime(2023, 12, 4, 17, 53, 15, 369, DateTimeKind.Local).AddTicks(4748), "user01@teste.com.br", "Usuario 01" },
                    { new Guid("5eeca3bb-864d-4886-8448-f8c53e2e10f9"), new DateTime(2023, 12, 4, 17, 53, 15, 369, DateTimeKind.Local).AddTicks(4762), "user03@teste.com.br", "Usuario 03" },
                    { new Guid("cfd685c7-ac90-479b-a2f4-3670aa26a015"), new DateTime(2023, 12, 4, 17, 53, 15, 369, DateTimeKind.Local).AddTicks(4760), "user02@teste.com.br", "Usuario 02" },
                    { new Guid("ee8dc968-e664-41d5-ab47-30050420f60a"), new DateTime(2023, 12, 4, 17, 53, 15, 369, DateTimeKind.Local).AddTicks(4810), "gestor@teste.com.br", "Gestor" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ComentarioTarefa_IdTarefa",
                table: "ComentarioTarefa",
                column: "IdTarefa");

            migrationBuilder.CreateIndex(
                name: "IX_ComentarioTarefa_IdUsuario",
                table: "ComentarioTarefa",
                column: "IdUsuario");

            migrationBuilder.CreateIndex(
                name: "IX_HistoricoAlteracaoTarefa_IdTarefa",
                table: "HistoricoAlteracaoTarefa",
                column: "IdTarefa");

            migrationBuilder.CreateIndex(
                name: "IX_HistoricoAlteracaoTarefa_IdUsuario",
                table: "HistoricoAlteracaoTarefa",
                column: "IdUsuario");

            migrationBuilder.CreateIndex(
                name: "IX_Projetos_IdUsuario",
                table: "Projetos",
                column: "IdUsuario");

            migrationBuilder.CreateIndex(
                name: "IX_Tarefas_IdProjeto",
                table: "Tarefas",
                column: "IdProjeto");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ComentarioTarefa");

            migrationBuilder.DropTable(
                name: "HistoricoAlteracaoTarefa");

            migrationBuilder.DropTable(
                name: "Tarefas");

            migrationBuilder.DropTable(
                name: "Projetos");

            migrationBuilder.DropTable(
                name: "Usuarios");
        }
    }
}
