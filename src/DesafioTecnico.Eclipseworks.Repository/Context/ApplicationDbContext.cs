﻿using DesafioTecnico.Eclipseworks.Domain.Projetos;
using DesafioTecnico.Eclipseworks.Domain.Tarefas;
using DesafioTecnico.Eclipseworks.Domain.Usuarios;
using DesafioTecnico.Eclipseworks.Repository.Map;
using Microsoft.EntityFrameworkCore;

namespace DesafioTecnico.Eclipseworks.Repository.Context;

public class ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : DbContext(options)
{
    public DbSet<Projeto> Projetos { get; set; }
    public DbSet<Tarefa> Tarefas { get; set; }
    public DbSet<Usuario> Usuarios { get; set; }
    public DbSet<HistoricoTarefa> HistoricoTarefas { get; set; }
    public DbSet<ComentarioTarefa> Comentarios { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfiguration(new ProjetoMap());
        modelBuilder.ApplyConfiguration(new ComentarioTarefaMap());
        modelBuilder.ApplyConfiguration(new HistoricoTarefaMap());
        modelBuilder.ApplyConfiguration(new TarefaMap());
        modelBuilder.ApplyConfiguration(new UsuarioMap());
    }
}
