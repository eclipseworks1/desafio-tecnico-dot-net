﻿using DesafioTecnico.Eclipseworks.Domain.Projetos.Repositories;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Repositories;
using DesafioTecnico.Eclipseworks.Repository.Projetos;
using DesafioTecnico.Eclipseworks.Repository.Tarefas;
using Microsoft.Extensions.DependencyInjection;

namespace DesafioTecnico.Eclipseworks.Repository.Extensions;

public static class DependencyInjectionExtensions
{
    public static IServiceCollection RegisterRepositoryDependencies(this IServiceCollection services)
    {
        services.AddTransient<ITarefaRepository, TarefaRepository>();
        services.AddTransient<IProjetoRepository, ProjetoRepository>();
        services.AddTransient<IMetricaProjetoRepository, MetricaProjetoRepository>();
        
        return services;
    }
}