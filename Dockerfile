#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS base
USER app
WORKDIR /app
EXPOSE 8080
EXPOSE 8081

FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
ARG BUILD_CONFIGURATION=Release
WORKDIR /src
COPY ["src/DesafioTecnico.Eclipseworks.Api/DesafioTecnico.Eclipseworks.Api.csproj", "src/DesafioTecnico.Eclipseworks.Api/"]
COPY ["src/DesafioTecnico.Eclipseworks.Business/DesafioTecnico.Eclipseworks.Services.csproj", "src/DesafioTecnico.Eclipseworks.Business/"]
COPY ["src/DesafioTecnico.Eclipseworks.Domain/DesafioTecnico.Eclipseworks.Domain.csproj", "src/DesafioTecnico.Eclipseworks.Domain/"]
COPY ["src/DesafioTecnico.Eclipseworks.Repository/DesafioTecnico.Eclipseworks.Repository.csproj", "src/DesafioTecnico.Eclipseworks.Repository/"]

RUN dotnet restore "./src/DesafioTecnico.Eclipseworks.Api/./DesafioTecnico.Eclipseworks.Api.csproj"
COPY . .
WORKDIR "/src/src/DesafioTecnico.Eclipseworks.Api"
RUN dotnet build "./DesafioTecnico.Eclipseworks.Api.csproj" -c $BUILD_CONFIGURATION -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "./DesafioTecnico.Eclipseworks.Api.csproj" -c $BUILD_CONFIGURATION -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "DesafioTecnico.Eclipseworks.Api.dll"]