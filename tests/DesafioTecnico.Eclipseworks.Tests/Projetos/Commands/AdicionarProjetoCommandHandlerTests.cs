﻿using DesafioTecnico.Eclipseworks.Business;
using DesafioTecnico.Eclipseworks.Domain;
using DesafioTecnico.Eclipseworks.Domain.Projetos;
using DesafioTecnico.Eclipseworks.Domain.Projetos.Commands;
using DesafioTecnico.Eclipseworks.Domain.Projetos.Repositories;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.AdicionarTarefa.Commands;
using DesafioTecnico.Eclipseworks.Services.Validators.Projetos;
using FluentAssertions;
using Moq;

namespace DesafioTecnico.Eclipseworks.Tests.Projetos.Commands;

public class AdicionarProjetoCommandHandlerTests
{
    private readonly AdicionarProjetoCommandHandler _commandHandler;

    private readonly Mock<IUser> _usuarioLogado = new();
    private readonly AdicionarProjetoValidator _validator = new();
    private readonly Mock<IProjetoRepository> _projetoRepository = new();
    private readonly IDomainNotificationAppService _domainNotificationAppService = new DomainNotificationAppService();

    public AdicionarProjetoCommandHandlerTests()
    {
        _commandHandler = new AdicionarProjetoCommandHandler(_domainNotificationAppService, _projetoRepository.Object, _usuarioLogado.Object, _validator);
    }

    [Fact]
    public async Task AdicionarProjetoCommandHandler_EntidadeOk_AdicionarUsuario()
    {
        // Arrange
        var projeto = new AdicionarProjetoCommand(Nome: "Projeto Testes");

        // Act
        await _commandHandler.Handle(projeto, CancellationToken.None);

        // Assert
        _projetoRepository.Verify(a => a.AdicionarAsync(It.IsAny<Projeto>()), Times.Once);
        _domainNotificationAppService.HasNotifications.Should().BeFalse();
    }

    [Theory]
    [InlineData("")]
    [InlineData(null)]
    [InlineData("NOME_COM_MAIS_CARACTERES_QUE_O_PERMITIDO_AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")]
    public async Task AdicionarProjetoCommandHandler_EntidadeInvalida_AdicionarRegrasNegocio(string nomeProjeto)
    {
        // Arrange
        var projeto = new AdicionarProjetoCommand(Nome: nomeProjeto);

        // Act
        await _commandHandler.Handle(projeto, CancellationToken.None);

        // Assert
        _projetoRepository.Verify(a => a.AdicionarAsync(It.IsAny<Projeto>()), Times.Never);
        _domainNotificationAppService.HasNotifications.Should().BeTrue();
    }
}
