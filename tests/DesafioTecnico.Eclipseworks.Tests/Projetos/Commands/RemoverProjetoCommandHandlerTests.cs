﻿using DesafioTecnico.Eclipseworks.Business;
using DesafioTecnico.Eclipseworks.Domain.Projetos.Commands;
using DesafioTecnico.Eclipseworks.Domain.Projetos.Repositories;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.AdicionarTarefa.Commands;
using DesafioTecnico.Eclipseworks.Services.Validators.Projetos;
using FluentAssertions;
using Moq;

namespace DesafioTecnico.Eclipseworks.Tests.Projetos.Commands;

public class RemoverProjetoCommandHandlerTests
{
    private readonly RemoverProjetoCommandHandler _commandHandler;
    private readonly IDomainNotificationAppService _domainNotificationAppService = new DomainNotificationAppService();

    private Mock<IProjetoRepository> _repository;
    private readonly RemoverProjetoValidator _validator;

    public RemoverProjetoCommandHandlerTests()
    {
        _repository = new Mock<IProjetoRepository>();
        _validator = new RemoverProjetoValidator(_repository.Object);
        _repository.Setup(a => a.ProjetoExisteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));
        _commandHandler = new RemoverProjetoCommandHandler(_domainNotificationAppService, _repository.Object, _validator);
    }

    [Fact]
    public async Task RemoverProjetoCommandHandler_ProjetoNaoPossuiTarefasPendentes_RemoverComSucesso()
    {
        // Arrange
        var idProjeto = Guid.NewGuid();
        var command = new RemoverProjetoCommand(idProjeto);
        _repository.Setup(a => a.PossuiTarefasPendentesAsync(idProjeto)).Returns(Task.FromResult(false));

        // Act
        await _commandHandler.Handle(command, CancellationToken.None);

        // Assert
        _repository.Verify(a => a.ExcluirAsync(idProjeto), Times.Once);
        _domainNotificationAppService.HasNotifications.Should().BeFalse();
    }

    [Fact]
    public async Task RemoverProjetoCommandHandler_ProjetoPossuiTarefasPendentes_AdicionarCritica()
    {
        // Arrange
        var idProjeto = Guid.NewGuid();
        var command = new RemoverProjetoCommand(idProjeto);
        _repository.Setup(a => a.PossuiTarefasPendentesAsync(idProjeto)).Returns(Task.FromResult(true));

        // Act
        await _commandHandler.Handle(command, CancellationToken.None);

        // Assert
        _repository.Verify(a => a.ExcluirAsync(idProjeto), Times.Never);
        _domainNotificationAppService.HasNotifications.Should().BeTrue();
        _validator.CriticasNegocio.Should().Contain("O projeto possui tarefas pendentes. Conclua as tarefas ou remova-as do projeto antes de excluí-lo.");
    }

    [Fact]
    public async Task RemoverProjetoCommandHandler_ProjetoNaoExiste_AdicionarCritica()
    {
        // Arrange
        var idProjeto = Guid.NewGuid();
        var command = new RemoverProjetoCommand(idProjeto);
        _repository.Setup(a => a.ProjetoExisteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(false));

        // Act
        await _commandHandler.Handle(command, CancellationToken.None);

        // Assert
        _repository.Verify(a => a.ExcluirAsync(idProjeto), Times.Never);
        _domainNotificationAppService.HasNotifications.Should().BeTrue();
        _validator.CriticasNegocio.Should().Contain("Operação inválida. Projeto não encontrado.");
    }
}
