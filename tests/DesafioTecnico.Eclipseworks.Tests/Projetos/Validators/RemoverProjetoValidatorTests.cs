﻿using DesafioTecnico.Eclipseworks.Domain.Projetos.Commands;
using DesafioTecnico.Eclipseworks.Domain.Projetos.Repositories;
using DesafioTecnico.Eclipseworks.Services.Validators.Projetos;
using FluentAssertions;
using Moq;

namespace DesafioTecnico.Eclipseworks.Tests.Projetos.Validators;

public class RemoverProjetoValidatorTests
{
    private readonly RemoverProjetoValidator _validator;
    private readonly Mock<IProjetoRepository> _projetoRepository = new();

    public RemoverProjetoValidatorTests()
    {
        _projetoRepository.Setup(a => a.ProjetoExisteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));
        _validator = new RemoverProjetoValidator(_projetoRepository.Object);
    }

    [Fact]
    public async Task RemoverProjetoValidatorTests_ProjetoNaoPossuiTarefasPendentes_RemoverComSucesso()
    {
        // Arrange
        var idProjeto = Guid.NewGuid();
        var command = new RemoverProjetoCommand(idProjeto);
        _projetoRepository.Setup(a => a.PossuiTarefasPendentesAsync(idProjeto)).Returns(Task.FromResult(false));

        // Act
        var valid = await _validator.OperacaoValida(command);

        // Assert
        valid.Should().BeTrue();
        _validator.CriticasNegocio.Should().BeEmpty();
    }

    [Fact]
    public async Task RemoverProjetoValidatorTests_ProjetoPossuiTarefasPendentes_AdicionarCritica()
    {
        // Arrange
        var idProjeto = Guid.NewGuid();
        var command = new RemoverProjetoCommand(idProjeto);
        _projetoRepository.Setup(a => a.PossuiTarefasPendentesAsync(idProjeto)).Returns(Task.FromResult(true));

        // Act
        var valid = await _validator.OperacaoValida(command);

        // Assert
        valid.Should().BeFalse();
        _validator.CriticasNegocio.Should().HaveCount(1);
        _validator.CriticasNegocio.Should().Contain("O projeto possui tarefas pendentes. Conclua as tarefas ou remova-as do projeto antes de excluí-lo.");
    }

    [Fact]
    public async Task RemoverProjetoValidatorTests_ProjetoNaoExiste_AdicionarCritica()
    {
        // Arrange
        var idProjeto = Guid.NewGuid();
        var command = new RemoverProjetoCommand(idProjeto);
        _projetoRepository.Setup(a => a.ProjetoExisteAsync(idProjeto)).Returns(Task.FromResult(false));

        // Act
        var valid = await _validator.OperacaoValida(command);

        // Assert
        valid.Should().BeFalse();
        _validator.CriticasNegocio.Should().HaveCount(1);
        _validator.CriticasNegocio.Should().Contain("Operação inválida. Projeto não encontrado.");
    }
}
