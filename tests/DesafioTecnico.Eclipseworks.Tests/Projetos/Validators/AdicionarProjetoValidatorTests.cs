﻿using DesafioTecnico.Eclipseworks.Domain.Projetos.Commands;
using DesafioTecnico.Eclipseworks.Services.Validators.Projetos;
using FluentAssertions;

namespace DesafioTecnico.Eclipseworks.Tests.Projetos.Validators;

public class AdicionarProjetoValidatorTests
{
    private readonly AdicionarProjetoValidator _validator;

    public AdicionarProjetoValidatorTests()
    {
        _validator = new();
    }

    [Fact]
    public void AdicionarProjetoValidatorTests_NomeOk_NaoAdicionarCriticas()
    {
        // Arrange
        var projeto = new AdicionarProjetoCommand(Nome: "Projeto Exemplo 01");

        // Act
        var valid = _validator.OperacaoValida(projeto);

        // Assert
        valid.Should().BeTrue();
        _validator.CriticasNegocio.Should().BeEmpty();
    }

    [Fact]
    public void AdicionarProjetoValidatorTests_NomeVazio_AdicionarCriticaCampoNome()
    {
        // Arrange
        var projeto = new AdicionarProjetoCommand(Nome: string.Empty);

        // Act
        var valid = _validator.OperacaoValida(projeto);

        // Assert
        valid.Should().BeFalse();
        _validator.CriticasNegocio.Should().HaveCount(1);
        _validator.CriticasNegocio.Should().Contain("O campo Nome é obrigatório.");
    }

    [Fact]
    public void AdicionarProjetoValidatorTests_NomeNull_AdicionarCriticaCampoNome()
    {
        // Arrange
        var projeto = new AdicionarProjetoCommand(Nome: null);

        // Act
        var valid = _validator.OperacaoValida(projeto);

        // Assert
        valid.Should().BeFalse();
        _validator.CriticasNegocio.Should().HaveCount(1);
        _validator.CriticasNegocio.Should().Contain("O campo Nome é obrigatório.");
    }

    [Fact]
    public void AdicionarProjetoValidatorTests_NomeComMaisCaracteresQuePermitido_AdicionarCriticaCampoNome()
    {
        // Arrange
        var nomeProjeto = new string('A', 256);
        var projeto = new AdicionarProjetoCommand(Nome: nomeProjeto);

        // Act
        var valid = _validator.OperacaoValida(projeto);

        // Assert
        valid.Should().BeFalse();
        _validator.CriticasNegocio.Should().HaveCount(1);
        _validator.CriticasNegocio.Should().Contain("O campo Nome deve possuir no máximo 255 caracteres.");
    }
}