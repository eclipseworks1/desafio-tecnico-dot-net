﻿using DesafioTecnico.Eclipseworks.Business;
using DesafioTecnico.Eclipseworks.Business.Tarefas.Commands;
using DesafioTecnico.Eclipseworks.Domain.Tarefas;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Commands;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Repositories;
using DesafioTecnico.Eclipseworks.Services.Validators.Tarefas;
using FluentAssertions;
using Moq;

namespace DesafioTecnico.Eclipseworks.Tests.Tarefas.Commands;

public class AtualizarTarefaCommandHandlerTests
{
    private readonly Mock<ITarefaRepository> _tarefaRepository = new();
    private readonly IDomainNotificationAppService _domainNotificationAppService = new DomainNotificationAppService();

    private readonly AtualizarTarefaValidator _validator;
    private readonly AtualizarTarefaCommandHandler _commandHandler;

    public AtualizarTarefaCommandHandlerTests()
    {
        _tarefaRepository.Setup(a => a.RecuperarPorIdAsync(It.IsAny<Guid>(), It.IsAny<Guid>())).Returns(Task.FromResult(new Tarefa()));
        _tarefaRepository.Setup(a => a.TarefaExiste(It.IsAny<Guid>(), It.IsAny<Guid>())).Returns(Task.FromResult(true));

        _validator = new AtualizarTarefaValidator(_tarefaRepository.Object);
        _commandHandler = new AtualizarTarefaCommandHandler(_domainNotificationAppService, _validator, _tarefaRepository.Object);
    }

    [Fact]
    public async Task AtualizarTarefaCommandHandlerTests_TarefaNaoExiste_AdicionarCritica()
    {
        // Arrange
        _tarefaRepository.Setup(a => a.TarefaExiste(It.IsAny<Guid>(), It.IsAny<Guid>())).Returns(Task.FromResult(false));

        // Act
        await _commandHandler.Handle(Tarefa, CancellationToken.None);

        // Assert
        _tarefaRepository.Verify(a => a.Adicionar(It.IsAny<Tarefa>()), Times.Never);
        _domainNotificationAppService.HasNotifications.Should().BeTrue();
        _validator.CriticasNegocio.Should().Contain("Operação inválida. Tarefa não encontrada.");
    }

    [Fact]
    public async Task AtualizarTarefaCommandHandlerTests_EntidadeOk_AdicionarTarefa()
    {
        // Arrange & Act
        await _commandHandler.Handle(Tarefa, CancellationToken.None);

        // Assert
        _tarefaRepository.Verify(a => a.Atualizar(It.IsAny<Tarefa>()), Times.Once);
        _domainNotificationAppService.HasNotifications.Should().BeFalse();
    }

    [Fact]
    public async Task AtualizarTarefaCommandHandlerTests_CamposObrigatoriosNull_AdicionarCriticas()
    {
        // Arrange
        var tarefa = Tarefa with { Descricao = string.Empty, Titulo = string.Empty };

        //  Act
        await _commandHandler.Handle(tarefa, CancellationToken.None);

        // Assert
        _tarefaRepository.Verify(a => a.Adicionar(It.IsAny<Tarefa>()), Times.Never);
        _domainNotificationAppService.HasNotifications.Should().BeTrue();
        _domainNotificationAppService.GetNotifications().Should().HaveCount(2);
        _domainNotificationAppService.GetNotifications().Should().Contain("O campo Titulo é obrigatório.");
        _domainNotificationAppService.GetNotifications().Should().Contain("O campo Descrição é obrigatório.");
    }

    [Fact]
    public async Task AtualizarTarefaCommandHandlerTests_CampoMaioresQuePermitico_AdicionarCriticas()
    {
        // Arrange
        var tarefa = Tarefa with { Descricao = new string('A', 10001), Titulo = new string('A', 256) };

        //  Act
        await _commandHandler.Handle(tarefa, CancellationToken.None);

        // Assert
        _tarefaRepository.Verify(a => a.Adicionar(It.IsAny<Tarefa>()), Times.Never);
        _domainNotificationAppService.HasNotifications.Should().BeTrue();
        _domainNotificationAppService.GetNotifications().Should().HaveCount(2);
        _domainNotificationAppService.GetNotifications().Should().Contain("O campo Descricao deve possuir no máximo 1000 caracteres.");
        _domainNotificationAppService.GetNotifications().Should().Contain("O campo Titulo deve possuir no máximo 255 caracteres.");
    }

    [Fact]
    public async Task AtualizarTarefaCommandHandlerTests_DataVencimentoTarefaNoPassado_AdicionarCriticas()
    {
        // Arrange
        var tarefa = Tarefa with { DataVencimento = DateTime.Now.AddDays(-1) };

        //  Act
        await _commandHandler.Handle(tarefa, CancellationToken.None);

        // Assert
        _tarefaRepository.Verify(a => a.Adicionar(It.IsAny<Tarefa>()), Times.Never);
        _domainNotificationAppService.HasNotifications.Should().BeTrue();
        _domainNotificationAppService.GetNotifications().Should().HaveCount(1);
        _domainNotificationAppService.GetNotifications().Should().Contain("O campo Data venciento deve ser maior que a data atual.");
    }

    [Theory]
    [InlineData(99)]
    [InlineData(-1)]
    [InlineData(4)]
    [InlineData(10)]
    public async Task AtualizarTarefaCommandHandlerTests_StatusInvalido_AdicionarCriticas(int statusTarefa)
    {
        // Arrange
        var tarefa = Tarefa with { Status = (StatusTarefa)statusTarefa };

        //  Act
        await _commandHandler.Handle(tarefa, CancellationToken.None);

        // Assert
        _tarefaRepository.Verify(a => a.Adicionar(It.IsAny<Tarefa>()), Times.Never);
        _domainNotificationAppService.HasNotifications.Should().BeTrue();
        _domainNotificationAppService.GetNotifications().Should().HaveCount(1);
        _domainNotificationAppService.GetNotifications().Should().Contain("O campo Status é Invalido.");
    }

    private static AtualizarTarefaCommand Tarefa => new(Guid.NewGuid(),Guid.NewGuid())
    {
        DataVencimento = DateTime.Now.AddDays(5),
        Descricao = "Tarefa testes",
        Status = StatusTarefa.Pendente,
        Titulo = "Tarefa testes"
    };
}
