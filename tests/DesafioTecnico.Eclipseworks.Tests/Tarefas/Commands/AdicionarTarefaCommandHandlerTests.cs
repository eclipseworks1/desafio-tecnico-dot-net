﻿using DesafioTecnico.Eclipseworks.Business;
using DesafioTecnico.Eclipseworks.Domain.Projetos.Repositories;
using DesafioTecnico.Eclipseworks.Domain.Tarefas;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.AdicionarTarefa.Commands;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Commands;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Repositories;
using DesafioTecnico.Eclipseworks.Services.Validators.Tarefas;
using FluentAssertions;
using Moq;

namespace DesafioTecnico.Eclipseworks.Tests.Tarefas.Commands;

public class AdicionarTarefaCommandHandlerTests
{
    private readonly Mock<ITarefaRepository> _tarefaRepository = new();
    private readonly Mock<IProjetoRepository> _projetoRepository = new();
    private readonly IDomainNotificationAppService _domainNotificationAppService = new DomainNotificationAppService();
    private readonly AdicionarTarefaCommandHandler _commandHandler;
    private readonly AdicionarTarefaValidator _validator;

    public AdicionarTarefaCommandHandlerTests()
    {
        _projetoRepository.Setup(a => a.ProjetoExisteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));

        _validator = new AdicionarTarefaValidator(_projetoRepository.Object);
        _commandHandler = new AdicionarTarefaCommandHandler(_domainNotificationAppService, _tarefaRepository.Object, _validator);
    }

    [Fact]
    public async Task AdicionarTarefaCommandHandlerTests_ProjetoNaoExiste_AdicionarCritica()
    {
        // Arrange
        _projetoRepository.Setup(a => a.ProjetoExisteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(false));

        // Act
        await _commandHandler.Handle(Tarefa, CancellationToken.None);

        // Assert
        _tarefaRepository.Verify(a => a.Adicionar(It.IsAny<Tarefa>()), Times.Never);
        _domainNotificationAppService.HasNotifications.Should().BeTrue();
        _validator.CriticasNegocio.Should().Contain("Operação inválida. Projeto não encontrado.");
    }

    [Fact]
    public async Task AdicionarTarefaCommandHandlerTests_EntidadeOk_AdicionarTarefa()
    {
        // Arrange & Act
        await _commandHandler.Handle(Tarefa, CancellationToken.None);

        // Assert
        _tarefaRepository.Verify(a => a.Adicionar(It.IsAny<Tarefa>()), Times.Once);
        _domainNotificationAppService.HasNotifications.Should().BeFalse();
    }

    [Fact]
    public async Task AdicionarTarefaCommandHandlerTests_CamposObrigatoriosNull_AdicionarCriticas()
    {
        // Arrange
        var tarefa = Tarefa with { Descricao = string.Empty, Titulo = string.Empty };

        //  Act
        await _commandHandler.Handle(tarefa, CancellationToken.None);

        // Assert
        _tarefaRepository.Verify(a => a.Adicionar(It.IsAny<Tarefa>()), Times.Never);
        _domainNotificationAppService.HasNotifications.Should().BeTrue();
        _domainNotificationAppService.GetNotifications().Should().HaveCount(2);
        _domainNotificationAppService.GetNotifications().Should().Contain("O campo Titulo é obrigatório.");
        _domainNotificationAppService.GetNotifications().Should().Contain("O campo Descrição é obrigatório.");
    }

    [Fact]
    public async Task AdicionarTarefaCommandHandlerTests_CampoMaioresQuePermitico_AdicionarCriticas()
    {
        // Arrange
        var tarefa = Tarefa with { Descricao = new string('A', 10001), Titulo = new string('A', 256) };

        //  Act
        await _commandHandler.Handle(tarefa, CancellationToken.None);

        // Assert
        _tarefaRepository.Verify(a => a.Adicionar(It.IsAny<Tarefa>()), Times.Never);
        _domainNotificationAppService.HasNotifications.Should().BeTrue();
        _domainNotificationAppService.GetNotifications().Should().HaveCount(2);
        _domainNotificationAppService.GetNotifications().Should().Contain("O campo Descricao deve possuir no máximo 1000 caracteres.");
        _domainNotificationAppService.GetNotifications().Should().Contain("O campo Titulo deve possuir no máximo 255 caracteres.");
    }

    [Fact]
    public async Task AdicionarTarefaCommandHandlerTests_DataVencimentoTarefaNoPassado_AdicionarCriticas()
    {
        // Arrange
        var tarefa = Tarefa with { DataVencimento = DateTime.Now.AddDays(-1) };

        //  Act
        await _commandHandler.Handle(tarefa, CancellationToken.None);

        // Assert
        _tarefaRepository.Verify(a => a.Adicionar(It.IsAny<Tarefa>()), Times.Never);
        _domainNotificationAppService.HasNotifications.Should().BeTrue();
        _domainNotificationAppService.GetNotifications().Should().HaveCount(1);
        _domainNotificationAppService.GetNotifications().Should().Contain("O campo Data venciento deve ser maior que a data atual.");
    }

    [Theory]
    [InlineData(99)]
    [InlineData(-1)]
    [InlineData(4)]
    [InlineData(10)]
    public async Task AdicionarTarefaCommandHandlerTests_StatusInvalido_AdicionarCriticas(int statusTarefa)
    {
        // Arrange
        var tarefa = Tarefa with { Status = (StatusTarefa)statusTarefa };

        //  Act
        await _commandHandler.Handle(tarefa, CancellationToken.None);

        // Assert
        _tarefaRepository.Verify(a => a.Adicionar(It.IsAny<Tarefa>()), Times.Never);
        _domainNotificationAppService.HasNotifications.Should().BeTrue();
        _domainNotificationAppService.GetNotifications().Should().HaveCount(1);
        _domainNotificationAppService.GetNotifications().Should().Contain("O campo Status é Invalido.");
    }

    [Theory]
    [InlineData(99)]
    [InlineData(-1)]
    [InlineData(4)]
    [InlineData(10)]
    public async Task AdicionarTarefaCommandHandlerTests_PrioridadeInvalida_AdicionarCriticas(int prioridade)
    {
        // Arrange
        var tarefa = Tarefa with { Prioridade = (PrioridadeTarefa)prioridade };

        //  Act
        await _commandHandler.Handle(tarefa, CancellationToken.None);

        // Assert
        _tarefaRepository.Verify(a => a.Adicionar(It.IsAny<Tarefa>()), Times.Never);
        _domainNotificationAppService.HasNotifications.Should().BeTrue();
        _domainNotificationAppService.GetNotifications().Should().HaveCount(1);
        _domainNotificationAppService.GetNotifications().Should().Contain("O campo Prioridade é Invalido.");
    }

    private static AdicionarTarefaCommand Tarefa => new(Guid.NewGuid())
    {
        DataVencimento = DateTime.Now.AddDays(5),
        Descricao = "Tarefa testes",
        Prioridade = PrioridadeTarefa.Media,
        Status = StatusTarefa.Pendente,
        Titulo = "Tarefa testes"
    };
}
