﻿using DesafioTecnico.Eclipseworks.Business;
using DesafioTecnico.Eclipseworks.Domain;
using DesafioTecnico.Eclipseworks.Domain.Tarefas;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Commands;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Repositories;
using DesafioTecnico.Eclipseworks.Services.Tarefas.Commands;
using DesafioTecnico.Eclipseworks.Services.Validators.Tarefas;
using FluentAssertions;
using Moq;

namespace DesafioTecnico.Eclipseworks.Tests.Tarefas.Commands
{
    public class AdicionarComentarioTarefaCommandHandlerTests
    {
        private readonly AdicionarComentarioTarefaValidator _validator;
        private readonly AdicionarComentarioTarefaCommandHandler _commandHandler;
        private readonly IDomainNotificationAppService _domainNotificationAppService = new DomainNotificationAppService();

        private readonly Mock<IUser> _usuario = new();
        private readonly Mock<ITarefaRepository> _tarefaRepository = new();

        public AdicionarComentarioTarefaCommandHandlerTests()
        {
            _validator = new AdicionarComentarioTarefaValidator(_tarefaRepository.Object);
            _tarefaRepository.Setup(a => a.TarefaExiste(It.IsAny<Guid>(), It.IsAny<Guid>())).Returns(Task.FromResult(true));
            _commandHandler = new AdicionarComentarioTarefaCommandHandler(_validator, _domainNotificationAppService, _tarefaRepository.Object, _usuario.Object);
        }

        [Fact]
        public async Task AdicionarComentarioTarefaCommandHandlerTests_OperacaoValida_AdicionarComentario()
        {
            // Act
            await _commandHandler.Handle(Command, CancellationToken.None);

            // Assert
            _domainNotificationAppService.HasNotifications.Should().BeFalse();
            _domainNotificationAppService.GetNotifications().Should().BeEmpty();

            _tarefaRepository.Verify(a => a.AdicionarComentario(It.IsAny<ComentarioTarefa>()),Times.Once);
        }

        [Fact]
        public async Task AdicionarComentarioTarefaCommandHandlerTests_TarefaNaoExiste_AdicionarCritica()
        {
             // Arrange
            _tarefaRepository.Setup(a => a.TarefaExiste(It.IsAny<Guid>(), It.IsAny<Guid>())).Returns(Task.FromResult(false));

            // Act
            await _commandHandler.Handle(Command, CancellationToken.None);

            // Assert
            _domainNotificationAppService.HasNotifications.Should().BeTrue();
            _domainNotificationAppService.GetNotifications().Should().HaveCount(1);
            _domainNotificationAppService.GetNotifications().Should().Contain("Operação inválida. Tarefa não encontrada.");

            _tarefaRepository.Verify(a => a.AdicionarComentario(It.IsAny<ComentarioTarefa>()), Times.Never);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task AdicionarComentarioTarefaCommandHandlerTests_CamposObrigatoriosNaoInformados_AdicionarCriticas(string comentario)
        {
            // Arrange
            var command = Command with { Comentario = comentario };

            // Act
            await _commandHandler.Handle(command, CancellationToken.None);

            // Assert
            _domainNotificationAppService.HasNotifications.Should().BeTrue();
            _domainNotificationAppService.GetNotifications().Should().HaveCount(1);
            _domainNotificationAppService.GetNotifications().Should().Contain("O campo Comentario é obrigatório.");

            _tarefaRepository.Verify(a => a.AdicionarComentario(It.IsAny<ComentarioTarefa>()), Times.Never);
        }

        [Fact]
        public async Task AdicionarComentarioTarefaCommandHandlerTests_CampoComMaisCaracteresQuePermitido_AdicionarCriticas()
        {
            // Arrange
            var command = Command with { Comentario = new string('A',256) };

            // Act
            await _commandHandler.Handle(command, CancellationToken.None);

            // Assert
            _domainNotificationAppService.HasNotifications.Should().BeTrue();
            _domainNotificationAppService.GetNotifications().Should().HaveCount(1);
            _domainNotificationAppService.GetNotifications().Should().Contain("O campo Comentario deve possuir no máximo 255 caracteres.");

            _tarefaRepository.Verify(a => a.AdicionarComentario(It.IsAny<ComentarioTarefa>()), Times.Never);
        }

        private AdicionarComentarioTarefaCommand Command = new(Guid.NewGuid(),Guid.NewGuid())
        {
            Comentario = "Comentario"
        };
    }
}
