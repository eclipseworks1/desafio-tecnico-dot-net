﻿using DesafioTecnico.Eclipseworks.Business;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.AdicionarTarefa.Commands;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Commands;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Repositories;
using DesafioTecnico.Eclipseworks.Services.Validators.Tarefas;
using FluentAssertions;
using Moq;

namespace DesafioTecnico.Eclipseworks.Tests.Tarefas.Commands
{
    public class RemoverTarefaCommandHandlerTests
    {
        private readonly RemoverTarefaValidator _validator;
        private readonly RemoverTarefaCommandHandler _commandHandler;
        private readonly IDomainNotificationAppService _domainNotificationAppService = new DomainNotificationAppService();

        private readonly Mock<ITarefaRepository> _tarefaRepository = new();

        public RemoverTarefaCommandHandlerTests()
        {
            _validator = new RemoverTarefaValidator(_tarefaRepository.Object);
            _tarefaRepository.Setup(a => a.TarefaExiste(It.IsAny<Guid>(), It.IsAny<Guid>())).Returns(Task.FromResult(true));
            _commandHandler = new RemoverTarefaCommandHandler(_validator, _domainNotificationAppService, _tarefaRepository.Object);
        }

        [Fact]
        public async Task RemoverTarefaCommandHandlerTests_OperacaoValida_RemoverTarefa()
        {
            // Act
            await _commandHandler.Handle(Command, CancellationToken.None);

            // Assert
            _domainNotificationAppService.HasNotifications.Should().BeFalse();
            _domainNotificationAppService.GetNotifications().Should().BeEmpty();

            _tarefaRepository.Verify(a => a.Excluir(It.IsAny<Guid>(), It.IsAny<Guid>()),Times.Once);
        }

        [Fact]
        public async Task RemoverTarefaCommandHandlerTests_TarefaNaoExiste_AdicionarCritica()
        {
             // Arrange
            _tarefaRepository.Setup(a => a.TarefaExiste(It.IsAny<Guid>(), It.IsAny<Guid>())).Returns(Task.FromResult(false));

            // Act
            await _commandHandler.Handle(Command, CancellationToken.None);

            // Assert
            _domainNotificationAppService.HasNotifications.Should().BeTrue();
            _domainNotificationAppService.GetNotifications().Should().HaveCount(1);
            _domainNotificationAppService.GetNotifications().Should().Contain("Operação inválida. Tarefa não encontrada.");

            _tarefaRepository.Verify(a => a.Excluir(It.IsAny<Guid>(), It.IsAny<Guid>()), Times.Never);
        }

        private readonly RemoverTarefaCommand Command = new(Guid.NewGuid(), Guid.NewGuid());
    }
}
