﻿using DesafioTecnico.Eclipseworks.Domain.Tarefas;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Commands;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Repositories;
using DesafioTecnico.Eclipseworks.Services.Validators.Tarefas;
using FluentAssertions;
using Moq;

namespace DesafioTecnico.Eclipseworks.Tests.Tarefas.Validators
{
    public class AtualizarTarefaValidatorTests
    {
        private readonly AtualizarTarefaValidator _validator;
        private readonly Mock<ITarefaRepository> _tarefaRepotisoy = new();

        public AtualizarTarefaValidatorTests()
        {
            _tarefaRepotisoy.Setup(a => a.TarefaExiste(It.IsAny<Guid>(),It.IsAny<Guid>())).Returns(Task.FromResult(true));            
            _validator = new AtualizarTarefaValidator(_tarefaRepotisoy.Object);
        }

        [Fact]
        public async Task AtualizarTarefaValidatorTests_TarefaValida_AdicionarCritica()
        {
            // Act
            var valido = await _validator.OperacaoValida(TarefaValida);

            // Assert
            valido.Should().BeTrue();
            _validator.CriticasNegocio.Should().BeEmpty();
        }

        [Fact]
        public async Task AtualizarTarefaValidatorTests_TarefaNaoExiste_AdicionarCritica()
        {
            // Arrange
            _tarefaRepotisoy.Setup(a => a.TarefaExiste(It.IsAny<Guid>(), It.IsAny<Guid>())).Returns(Task.FromResult(false));

            // Act
            var valido = await _validator.OperacaoValida(TarefaValida);

            // Assert
            valido.Should().BeFalse();
            _validator.CriticasNegocio.Should().HaveCount(1);
            _validator.CriticasNegocio.Should().Contain("Operação inválida. Tarefa não encontrada.");
        }

        [Theory]
        [InlineData("", "")]
        [InlineData(null, null)]
        public async Task AtualizarTarefaValidatorTests_CamposObrigatoriosNaoInformados_AdicionarCriticas(string titulo, string descricao)
        {
            // Arrange
            var tarefa = TarefaValida with { Titulo = titulo, Descricao = descricao };

            // Act
            var valido = await _validator.OperacaoValida(tarefa);

            // Assert
            valido.Should().BeFalse();
            _validator.CriticasNegocio.Should().HaveCount(2);
            _validator.CriticasNegocio.Should().Contain("O campo Titulo é obrigatório.");
            _validator.CriticasNegocio.Should().Contain("O campo Descrição é obrigatório.");
        }

        [Fact]
        public async Task AtualizarTarefaValidatorTests_CamposMaisCaracteresPermitido_AdicionarCriticas()
        {
            // Arrange
            var tarefa = TarefaValida with { Titulo = new string('A',256), Descricao = new string('A',1001)};

            // Act
            var valido = await _validator.OperacaoValida(tarefa);

            // Assert
            valido.Should().BeFalse();
            _validator.CriticasNegocio.Should().HaveCount(2);
            _validator.CriticasNegocio.Should().Contain("O campo Titulo deve possuir no máximo 255 caracteres.");
            _validator.CriticasNegocio.Should().Contain("O campo Descricao deve possuir no máximo 1000 caracteres.");
        }

        [Fact]
        public async Task AtualizarTarefaValidatorTests_DataVencimentoAnteriorHoje_AdicionarCritica()
        {
            // Arrange
            var tarefa = TarefaValida with {DataVencimento = DateTime.Now.AddDays(-1) };

            // Act
            var valido = await _validator.OperacaoValida(tarefa);

            // Assert
            valido.Should().BeFalse();
            _validator.CriticasNegocio.Should().HaveCount(1);
            _validator.CriticasNegocio.Should().Contain("O campo Data venciento deve ser maior que a data atual.");
        }

        [Theory]
        [InlineData(99)]
        [InlineData(-1)]
        [InlineData(4)]
        [InlineData(10)]
        public async Task AtualizarTarefaValidatorTests_StatusInvalido_AdicionarCritica(int status)
        {
            // Arrange
            var tarefa = TarefaValida with { Status = (StatusTarefa)status };

            // Act
            var valido = await _validator.OperacaoValida(tarefa);

            // Assert
            valido.Should().BeFalse();
            _validator.CriticasNegocio.Should().HaveCount(1);
            _validator.CriticasNegocio.Should().Contain("O campo Status é Invalido.");
        }

        private static AtualizarTarefaCommand TarefaValida => new(Guid.NewGuid(), Guid.NewGuid())
        {
            DataVencimento = DateTime.Now.AddDays(5),
            Descricao = "Tarefa testes",
            Status = StatusTarefa.Pendente,
            Titulo = "Tarefa testes"
        };
    }
}
