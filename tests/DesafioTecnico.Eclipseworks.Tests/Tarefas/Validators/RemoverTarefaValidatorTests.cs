﻿using DesafioTecnico.Eclipseworks.Domain.Tarefas.Commands;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Repositories;
using DesafioTecnico.Eclipseworks.Services.Validators.Tarefas;
using FluentAssertions;
using Moq;

namespace DesafioTecnico.Eclipseworks.Tests.Tarefas.Validators;

public class RemoverTarefaValidatorTests
{
    private Mock<ITarefaRepository> _tarefaRepository = new();
    private readonly AdicionarComentarioTarefaValidator _validator;

    public RemoverTarefaValidatorTests()
    {
        _tarefaRepository.Setup(a => a.TarefaExiste(It.IsAny<Guid>(), It.IsAny<Guid>())).Returns(Task.FromResult(true));
        _validator = new AdicionarComentarioTarefaValidator(_tarefaRepository.Object);
    }

    [Fact]
    public async Task RemoverTarefaValidatorTests_TarefaExiste_Excluir()
    {
        var command = new AdicionarComentarioTarefaCommand(Guid.NewGuid(), Guid.NewGuid())
        {
            Comentario = "Comentario TESTE"
        };

        // Act
        var operacaoValida = await _validator.OperacaoValida(command);

        // Assert
        operacaoValida.Should().BeTrue();
        _validator.CriticasNegocio.Should().BeEmpty();
    }

    [Fact]
    public async Task RemoverTarefaValidatorTests_TarefaNaoExiste_AdicionarCritica()
    {
        // Arrange
        _tarefaRepository.Setup(a => a.TarefaExiste(It.IsAny<Guid>(), It.IsAny<Guid>())).Returns(Task.FromResult(false));

        var command = new AdicionarComentarioTarefaCommand(Guid.NewGuid(), Guid.NewGuid())
        {
            Comentario = "Comentario TESTE"
        };

        // Act
        var operacaoValida = await _validator.OperacaoValida(command);

        // Assert
        operacaoValida.Should().BeFalse();
        _validator.CriticasNegocio.Should().HaveCount(1);
        _validator.CriticasNegocio.Should().Contain("Operação inválida. Tarefa não encontrada.");
    }
}