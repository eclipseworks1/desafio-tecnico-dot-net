﻿using DesafioTecnico.Eclipseworks.Domain.Tarefas.Commands;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Repositories;
using DesafioTecnico.Eclipseworks.Services.Validators.Tarefas;
using FluentAssertions;
using Moq;

namespace DesafioTecnico.Eclipseworks.Tests.Tarefas.Validators;

public class AdicionarComentarioTarefaValidatorTests
{
    private Mock<ITarefaRepository> _tarefaRepository = new();

    private readonly AdicionarComentarioTarefaValidator _validator;

    public AdicionarComentarioTarefaValidatorTests()
    {
        _tarefaRepository.Setup(a => a.TarefaExiste(It.IsAny<Guid>(), It.IsAny<Guid>())).Returns(Task.FromResult(true));
        _validator = new AdicionarComentarioTarefaValidator(_tarefaRepository.Object);
    }

    [Fact]
    public async Task AdicionarComentarioTarefaValidatorTests_TarefaNaoExiste_AdicionarCritica()
    {
        // Arrange
        _tarefaRepository.Setup(a => a.TarefaExiste(It.IsAny<Guid>(), It.IsAny<Guid>())).Returns(Task.FromResult(false));

        var command = new AdicionarComentarioTarefaCommand(Guid.NewGuid(), Guid.NewGuid())
        {
            Comentario = "Comentario TESTE"
        };

        // Act
        var operacaoValida = await _validator.OperacaoValida(command);

        // Assert
        operacaoValida.Should().BeFalse();
        _validator.CriticasNegocio.Should().HaveCount(1);
        _validator.CriticasNegocio.Should().Contain("Operação inválida. Tarefa não encontrada.");
    }

    [Theory]
    [InlineData(null)]
    [InlineData("")]
    public async Task AdicionarComentarioTarefaValidatorTests_ComentarioNaoPrenchido_AdicionarCritica(string comentario)
    {
        // Arrange
        var command = new AdicionarComentarioTarefaCommand(Guid.NewGuid(), Guid.NewGuid())
        {
            Comentario = comentario
        };

        // Act
        var operacaoValida = await _validator.OperacaoValida(command);

        // Assert
        operacaoValida.Should().BeFalse();
        _validator.CriticasNegocio.Should().HaveCount(1);
        _validator.CriticasNegocio.Should().Contain("O campo Comentario é obrigatório.");
    }

    [Fact]
    public async Task AdicionarComentarioTarefaValidatorTests_CampoMaioresQuePermitico_AdicionarCriticas()
    {
        // Arrange
        var command = new AdicionarComentarioTarefaCommand(Guid.NewGuid(), Guid.NewGuid())
        {
            Comentario = new string('A', count: 501)
        };

        // Act
        var operacaoValida = await _validator.OperacaoValida(command);

        // Assert
        operacaoValida.Should().BeFalse();
        _validator.CriticasNegocio.Should().HaveCount(1);
        _validator.CriticasNegocio.Should().Contain("O campo Comentario deve possuir no máximo 255 caracteres.");
    }
}