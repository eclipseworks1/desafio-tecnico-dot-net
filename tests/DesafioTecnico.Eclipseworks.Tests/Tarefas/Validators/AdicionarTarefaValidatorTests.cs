﻿using DesafioTecnico.Eclipseworks.Domain.Projetos.Repositories;
using DesafioTecnico.Eclipseworks.Domain.Tarefas;
using DesafioTecnico.Eclipseworks.Domain.Tarefas.Commands;
using DesafioTecnico.Eclipseworks.Services.Validators.Tarefas;
using FluentAssertions;
using Moq;

namespace DesafioTecnico.Eclipseworks.Tests.Tarefas.Validators
{
    public class AdicionarTarefaValidatorTests
    {
        private readonly AdicionarTarefaValidator _validator;
        private readonly Mock<IProjetoRepository> _projetoRepositor = new();

        public AdicionarTarefaValidatorTests()
        {
            _projetoRepositor.Setup(a => a.ProjetoExisteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(true));
            _projetoRepositor.Setup(a => a.RecuperarTotalTarefasProjetoAsync(It.IsAny<Guid>())).Returns(Task.FromResult(1));
            
            _validator = new AdicionarTarefaValidator(_projetoRepositor.Object);
        }

        [Fact]
        public async Task AdicionarTarefaValidatorTests_TarefaValida_AdicionarCritica()
        {
            // Act
            var valido = await _validator.OperacaoValida(TarefaValida);

            // Assert
            valido.Should().BeTrue();
            _validator.CriticasNegocio.Should().BeEmpty();
        }

        [Fact]
        public async Task AdicionarTarefaValidatorTests_ProjetoNaoExiste_AdicionarCritica()
        {
            // Arrange
            _projetoRepositor.Setup(a => a.ProjetoExisteAsync(It.IsAny<Guid>())).Returns(Task.FromResult(false));

            // Act
            var valido = await _validator.OperacaoValida(TarefaValida);

            // Assert
            valido.Should().BeFalse();
            _validator.CriticasNegocio.Should().HaveCount(1);
            _validator.CriticasNegocio.Should().Contain("Operação inválida. Projeto não encontrado.");
        }

        [Theory]
        [InlineData("", "")]
        [InlineData(null, null)]
        public async Task AdicionarTarefaValidatorTests_CamposObrigatoriosNaoInformados_AdicionarCriticas(string titulo, string descricao)
        {
            // Arrange
            var tarefa = TarefaValida with { Titulo = titulo, Descricao = descricao };

            // Act
            var valido = await _validator.OperacaoValida(tarefa);

            // Assert
            valido.Should().BeFalse();
            _validator.CriticasNegocio.Should().HaveCount(2);
            _validator.CriticasNegocio.Should().Contain("O campo Titulo é obrigatório.");
            _validator.CriticasNegocio.Should().Contain("O campo Descrição é obrigatório.");
        }

        [Fact]
        public async Task AdicionarTarefaValidatorTests_CamposMaisCaracteresPermitido_AdicionarCriticas()
        {
            // Arrange
            var tarefa = TarefaValida with { Titulo = new string('A',256), Descricao = new string('A',1001)};

            // Act
            var valido = await _validator.OperacaoValida(tarefa);

            // Assert
            valido.Should().BeFalse();
            _validator.CriticasNegocio.Should().HaveCount(2);
            _validator.CriticasNegocio.Should().Contain("O campo Titulo deve possuir no máximo 255 caracteres.");
            _validator.CriticasNegocio.Should().Contain("O campo Descricao deve possuir no máximo 1000 caracteres.");
        }

        [Fact]
        public async Task AdicionarTarefaValidatorTests_DataVencimentoAnteriorHoje_AdicionarCritica()
        {
            // Arrange
            var tarefa = TarefaValida with {DataVencimento = DateTime.Now.AddDays(-1) };

            // Act
            var valido = await _validator.OperacaoValida(tarefa);

            // Assert
            valido.Should().BeFalse();
            _validator.CriticasNegocio.Should().HaveCount(1);
            _validator.CriticasNegocio.Should().Contain("O campo Data venciento deve ser maior que a data atual.");
        }

        [Theory]
        [InlineData(99)]
        [InlineData(-1)]
        [InlineData(4)]
        [InlineData(10)]
        public async Task AdicionarTarefaValidatorTests_StatusInvalido_AdicionarCritica(int status)
        {
            // Arrange
            var tarefa = TarefaValida with { Status = (StatusTarefa)status };

            // Act
            var valido = await _validator.OperacaoValida(tarefa);

            // Assert
            valido.Should().BeFalse();
            _validator.CriticasNegocio.Should().HaveCount(1);
            _validator.CriticasNegocio.Should().Contain("O campo Status é Invalido.");
        }

        [Theory]
        [InlineData(99)]
        [InlineData(-1)]
        [InlineData(4)]
        [InlineData(10)]
        public async Task AdicionarTarefaValidatorTests_PrioridadeInvalida_AdicionarCritica(int prioridade)
        {
            // Arrange
            var tarefa = TarefaValida with { Prioridade = (PrioridadeTarefa)prioridade};

            // Act
            var valido = await _validator.OperacaoValida(tarefa);

            // Assert
            valido.Should().BeFalse();
            _validator.CriticasNegocio.Should().HaveCount(1);
            _validator.CriticasNegocio.Should().Contain("O campo Prioridade é Invalido.");
        }

        [Fact]
        public async Task AdicionarTarefaValidatorTests_ProjetoPossui20TarefasAdicionadas_AdicionarCritica()
        {
            // Arrange
              _projetoRepositor.Setup(a => a.RecuperarTotalTarefasProjetoAsync(It.IsAny<Guid>())).Returns(Task.FromResult(20));

            // Act
            var valido = await _validator.OperacaoValida(TarefaValida);

            // Assert
            valido.Should().BeFalse();
            _validator.CriticasNegocio.Should().HaveCount(1);
            _validator.CriticasNegocio.Should().Contain("O número máximo de tarefas por projeto é 20.");
        }

        private static AdicionarTarefaCommand TarefaValida => new(Guid.NewGuid())
        {
            DataVencimento = DateTime.Now.AddDays(5),
            Descricao = "Tarefa testes",
            Prioridade = PrioridadeTarefa.Media,
            Status = StatusTarefa.Pendente,
            Titulo = "Tarefa testes"
        };
    }
}
