﻿/* Atenção - Projeto criado somente para auxilio dos testes da api */


// O descritivo do desafio tecnico informa que não é necessario implementar autenticacao na aplicação ( será um serviço externo )
// O objetivo desse projeto é auxiliar durante o desenvolvimento, fornecendo tokens no padrão JWT, simulando o que será enviado após autenticacao externa.

// Metodos apenas para auxilio do desenvolvedor. Este projeto não foi criado para nenhum tipo de uso em ambiente produtivo.

GerarJwtToken("55b92a2e-def1-43db-a50f-102e2287d388", email: "user01@teste.com.br");
GerarJwtToken("cfd685c7-ac90-479b-a2f4-3670aa26a015", email: "user02@teste.com.br");
GerarJwtToken("5eeca3bb-864d-4886-8448-f8c53e2e10f9", email: "user03@teste.com.br");
GerarJwtToken("4c3017dc-ee78-4fb3-802f-20cb43323a51", email: "user04@teste.com.br");
GerarJwtToken("2b403426-6eb6-4485-89bd-36e67ea0e5f3", email: "user05@teste.com.br");

var gerente = new Claim("PerfilAcesso", "Gerente");
GerarJwtToken("ee8dc968-e664-41d5-ab47-30050420f60a", email: "gestor@teste.com.br", gerente);

static string GerarJwtToken(string id, string email, Claim? claim = null)
{
    var idUsuario = Guid.Parse(id);

    var secretKey = ApplicationSecurity.SecretKey;
    var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(secretKey));

    var claims = new List<Claim>
    {
        new Claim(JwtRegisteredClaimNames.Sub, idUsuario.ToString()),
        new Claim(JwtRegisteredClaimNames.Email, email),
        new Claim(JwtRegisteredClaimNames.UniqueName, email)
    };

    if (claim != null)
    {
        claims.Add(claim);
    }

    var credencial = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

    var token = new JwtSecurityToken(
        issuer: ApplicationSecurity.Issuer,
        audience: ApplicationSecurity.Audience,
        claims: claims,
        expires: DateTime.Now.AddDays(ApplicationSecurity.TotalDiasExpiracaoToken),
        signingCredentials: credencial
    );

    var jwt = new JwtSecurityTokenHandler().WriteToken(token);

    Console.WriteLine($"\n Usuario: {email}. Token => {jwt}");

    return jwt;
}