# Desafio Técnico EclipseWorks - Marcos Freire

## Características Técnicas do Projeto

O projeto utiliza as seguintes tecnologias e ferramentas:

- **.NET Core 8 (LTS)**
- **FluentAssertions**
- **FluentValidator**
- **Mediator**
- **Testes de unidade com Xunit**
- **Base de dados SQL Server**

## Instruções para Executar o Projeto

O projeto está configurado para execução no Docker. Siga as instruções abaixo no seu terminal:

```bash
# Via terminal, navegue até o diretorio raiz do projeto
# Certifique-se de ter o docker e docker compose instalado e execute o comando abaixo:

docker compose up

# A aplicação irá executar em: http://localhost:8080/swagger/index.html
# Ps : Importante lembrar que a primeira execução é mais lenta, devido ao pulling de imagens e build da imagem docker da aplicacao.

```

# JWT
Os Tokens abaixo são validos por 30 dias a partir 05/12/2023 de e servem para testes da api (collection do postman em anexo /docs/desafio-tecnico-eclipse-works.postman_collection.json)
/
 ### Usuario: user01@teste.com.br

 ```
 eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1NWI5MmEyZS1kZWYxLTQzZGItYTUwZi0xMDJlMjI4N2QzODgiLCJlbWFpbCI6InVzZXIwMUB0ZXN0ZS5jb20uYnIiLCJ1bmlxdWVfbmFtZSI6InVzZXIwMUB0ZXN0ZS5jb20uYnIiLCJleHAiOjE3MDQzNDYwNzQsImlzcyI6ImVjbGlwc2V3b3JrcyIsImF1ZCI6ImVjbGlwc2V3b3JrcyJ9.uqqdMbu0XK5AATR7C7JaAdffTkfqJMycK2JykwQsXWA
 
 ```
 ### Usuario: user02@teste.com.br
  ```
  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJjZmQ2ODVjNy1hYzkwLTQ3OWItYTJmNC0zNjcwYWEyNmEwMTUiLCJlbWFpbCI6InVzZXIwMkB0ZXN0ZS5jb20uYnIiLCJ1bmlxdWVfbmFtZSI6InVzZXIwMkB0ZXN0ZS5jb20uYnIiLCJleHAiOjE3MDQzNDYwNzUsImlzcyI6ImVjbGlwc2V3b3JrcyIsImF1ZCI6ImVjbGlwc2V3b3JrcyJ9.SO7P6VBpFHa6Ts2budoOodUwHbQQ5hmGtJTZGDucA0g
 ```

 ### Usuario: user03@teste.com.br
  ```
  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1ZWVjYTNiYi04NjRkLTQ4ODYtODQ0OC1mOGM1M2UyZTEwZjkiLCJlbWFpbCI6InVzZXIwM0B0ZXN0ZS5jb20uYnIiLCJ1bmlxdWVfbmFtZSI6InVzZXIwM0B0ZXN0ZS5jb20uYnIiLCJleHAiOjE3MDQzNDYwNzUsImlzcyI6ImVjbGlwc2V3b3JrcyIsImF1ZCI6ImVjbGlwc2V3b3JrcyJ9.CGzZ6gyPhVY2JAdQ6jI6wp11bldRug8WKHd2o8U57yE
 ```

 ### Usuario: user04@teste.com.br
  ```
  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI0YzMwMTdkYy1lZTc4LTRmYjMtODAyZi0yMGNiNDMzMjNhNTEiLCJlbWFpbCI6InVzZXIwNEB0ZXN0ZS5jb20uYnIiLCJ1bmlxdWVfbmFtZSI6InVzZXIwNEB0ZXN0ZS5jb20uYnIiLCJleHAiOjE3MDQzNDYwNzUsImlzcyI6ImVjbGlwc2V3b3JrcyIsImF1ZCI6ImVjbGlwc2V3b3JrcyJ9.68tvc1WYZxTtLNqAPRCa70hIEdTc2wRdE5TMHBOLBz4
 ```

 ### Usuario: user05@teste.com.br
  ```
  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyYjQwMzQyNi02ZWI2LTQ0ODUtODliZC0zNmU2N2VhMGU1ZjMiLCJlbWFpbCI6InVzZXIwNUB0ZXN0ZS5jb20uYnIiLCJ1bmlxdWVfbmFtZSI6InVzZXIwNUB0ZXN0ZS5jb20uYnIiLCJleHAiOjE3MDQzNDYwNzUsImlzcyI6ImVjbGlwc2V3b3JrcyIsImF1ZCI6ImVjbGlwc2V3b3JrcyJ9.CKakiH05NgIR_QwGRj1T1SBR4H-YHOUyEfP-BPxj69Q
 ```

## Usuario GERENTE -  gestor@teste.com.br
  ```
   eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJlZThkYzk2OC1lNjY0LTQxZDUtYWI0Ny0zMDA1MDQyMGY2MGEiLCJlbWFpbCI6Imdlc3RvckB0ZXN0ZS5jb20uYnIiLCJ1bmlxdWVfbmFtZSI6Imdlc3RvckB0ZXN0ZS5jb20uYnIiLCJQZXJmaWxBY2Vzc28iOiJHZXJlbnRlIiwiZXhwIjoxNzA0MzQ2MDc1LCJpc3MiOiJlY2xpcHNld29ya3MiLCJhdWQiOiJlY2xpcHNld29ya3MifQ.iNQgE2M4IT-w6Y9Adj1OCxWCnCrh_yXNJEXtdql1iyo
 ```

# Renovar tokens - JWT
A aplicação conta com autenticação externa, e portanto para o desafio tecnico os tokens estão mocados.
Caso os tokens fornecidos expirem, basta marcar o projeto **DesafioTecnico.Eclipseworks.Tests.TokenGenerator** como Startup no visual studio e executar a aplicação.
Serão gerados novos tokens validos para testes via swagger/postman/etc...


# Collection POSTMAN
Foi disponibilizado em /docs/desafio-tecnico-eclipse-works.postman_collection.json
Para utiliza-la, é necessario **alterar os Ids fornecidos nas rotas**

# Refinamento

Segue algumas perguntas que poderiam ser feitas ao Product Owner (PO) para enriquecer o projeto:

- É interessante adicionar a opção para alterar o projeto? (No descritivo da primeira sprint, não existe essa opção)
- É interessante adicionar a funcionalidade para listar o historico de atualizacao de tarefas para o usuario gerente?
- Seria útil incluir um percentual de tarefas concluídas por projeto?
- Deveria ser adicionado um limite para comentários por tarefa?
- Existe um limite máximo de projetos que um usuário pode criar?
- Seria interessante adicionar notificação de alteração do projeto/tarefas por email?
- Adicionar um checklist para a tarefa seria útil?

# Final

Segue um descritivo de possíveis melhorias futuras para o projeto:

- O projeto atualmente utiliza um pipeline para escrita de dados (commands) e um pipeline para leitura (queries) na mesma base de dados. Caso seja necessário melhoria de desempenho, uma opção é separar a base de leitura e a base de escrita.

- Não há implementação de paginação no projeto. Para futuras sprints, seria interessante adicionar histórias para paginar as respostas, a fim de ganhar desempenho e velocidade no carregamento de dados.

- Considerar a criação de endpoints para CRUD de usuário.